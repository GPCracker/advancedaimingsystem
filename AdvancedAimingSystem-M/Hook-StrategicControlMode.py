#*****
# StrategicControlMode Hooks
#*****
def new_StrategicControlMode_enable(self, **args):
	result = old_StrategicControlMode_enable(self, **args)
	#unlock height
	self.XLockedHeight = None
	#text info GUI
	if _config_['strategicAS']['heightLock']['heightInfoGUI']['enabled']:
		if not hasattr(self, 'XHeightInfoGUI') or self.XHeightInfoGUI is None:
			#init GUI
			self.XHeightInfoGUI = GUIWrapper.TextGUI(_config_['GUIDefaultSettings']['infoTextGUI'])
			self.XHeightInfoGUI.gui.position = _config_['strategicAS']['heightLock']['heightInfoGUI']['position']
			self.XHeightInfoGUI.gui.colour = _config_['strategicAS']['heightLock']['heightInfoGUI']['colour']
			self.XHeightInfoGUI.append()
		if hasattr(self, 'XHeightInfoGUI') and self.XHeightInfoGUI is not None:
			#show GUI
			self.XHeightInfoGUI.gui.visible = True
	if _config_['strategicAS']['targetLock']['targetInfoGUI']['enabled']:
		if not hasattr(self, 'XTargetInfoGUI') or self.XTargetInfoGUI is None:
			#init GUI
			self.XTargetInfoGUI = GUIWrapper.TextGUI(_config_['GUIDefaultSettings']['infoTextGUI'])
			self.XTargetInfoGUI.gui.position = _config_['strategicAS']['targetLock']['targetInfoGUI']['position']
			self.XTargetInfoGUI.gui.colour = _config_['strategicAS']['targetLock']['targetInfoGUI']['colour']
			self.XTargetInfoGUI.append()
		if hasattr(self, 'XTargetInfoGUI') and self.XTargetInfoGUI is not None:
			#show GUI
			self.XTargetInfoGUI.gui.visible = True
	#Aiming info GUI
	if _config_['strategicAS']['aimingInfo']['enabled']:
		if not hasattr(self, 'XAimingInfo') or self.XAimingInfo is None:
			#init GUI
			self.XAimingInfo = AimingInfo(_config_['GUIDefaultSettings']['aimingInfoWindow'], _config_['GUIDefaultSettings']['aimingInfoText'])
			self.XAimingInfo.window.textureName = _config_['strategicAS']['aimingInfo']['bgTextureName']
			self.XAimingInfo.window.colour = _config_['strategicAS']['aimingInfo']['bgAddColour']
			self.XAimingInfo.window.size = _config_['strategicAS']['aimingInfo']['windowSize']
			self.XAimingInfo.window.position = _config_['strategicAS']['aimingInfo']['windowPosition']
			self.XAimingInfo.window.label.colour = _config_['strategicAS']['aimingInfo']['labelColour']
		if hasattr(self, 'XAimingInfo') and self.XAimingInfo is not None:
			#show GUI
			self.XAimingInfo.window.visible = _config_['strategicAS']['aimingInfo']['activated']
	return result

def new_StrategicControlMode_disable(self):
	result = old_StrategicControlMode_disable(self)
	#unlock height
	self.XLockedHeight = None
	#text info GUI
	if _config_['strategicAS']['heightLock']['heightInfoGUI']['enabled']:
		if hasattr(self, 'XHeightInfoGUI') and self.XHeightInfoGUI is not None:
			#hide GUI
			self.XHeightInfoGUI.gui.visible = False
	if _config_['strategicAS']['targetLock']['targetInfoGUI']['enabled']:
		if hasattr(self, 'XTargetInfoGUI') and self.XTargetInfoGUI is not None:
			#hide GUI
			self.XTargetInfoGUI.gui.visible = False
	#Aiming info GUI
	if _config_['strategicAS']['aimingInfo']['enabled']:
		if hasattr(self, 'XAimingInfo') and self.XAimingInfo is not None:
			#hide GUI
			self.XAimingInfo.window.visible = False
	return result

def new_StrategicControlMode_getDesiredShotPoint(self):
	result = old_StrategicControlMode_getDesiredShotPoint(self)
	return result
