#*****
# ArcadeControlMode Hooks
#*****
def new_ArcadeControlMode_enable(self, **args):
	result = old_ArcadeControlMode_enable(self, **args)
	#Aiming info GUI
	if _config_['arcadeAS']['aimingInfo']['enabled']:
		if not hasattr(self, 'XAimingInfo') or self.XAimingInfo is None:
			#init GUI
			self.XAimingInfo = AimingInfo(_config_['GUIDefaultSettings']['aimingInfoWindow'], _config_['GUIDefaultSettings']['aimingInfoText'])
			self.XAimingInfo.window.textureName = _config_['arcadeAS']['aimingInfo']['bgTextureName']
			self.XAimingInfo.window.colour = _config_['arcadeAS']['aimingInfo']['bgAddColour']
			self.XAimingInfo.window.size = _config_['arcadeAS']['aimingInfo']['windowSize']
			self.XAimingInfo.window.position = _config_['arcadeAS']['aimingInfo']['windowPosition']
			self.XAimingInfo.window.label.colour = _config_['arcadeAS']['aimingInfo']['labelColour']
		if hasattr(self, 'XAimingInfo') and self.XAimingInfo is not None:
			#show GUI
			self.XAimingInfo.window.visible = _config_['arcadeAS']['aimingInfo']['activated']
	return result

def new_ArcadeControlMode_disable(self):
	result = old_ArcadeControlMode_disable(self)
	#Aiming info GUI
	if _config_['arcadeAS']['aimingInfo']['enabled']:
		if hasattr(self, 'XAimingInfo') and self.XAimingInfo is not None:
			#hide GUI
			self.XAimingInfo.window.visible = False
	return result
