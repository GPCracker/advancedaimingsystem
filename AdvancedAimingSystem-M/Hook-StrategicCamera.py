#*****
# StrategicCamera Hooks
#*****
def new_StrategicCamera_enable(self, targetPos, saveDist):
	result = old_StrategicCamera_enable(self, targetPos, saveDist)
	self.aimingSystem.XViewMatrix = Math.Matrix()
	self.aimingSystem.XRangeCollider = RangeCollider()
	self.aimingSystem.XCameraBasePitch = _config_['strategicAS']['strategicSniperMode']['cameraBasePitch']
	if _config_['strategicAS']['strategicSniperMode']['enabled']:
		_config_['strategicAS']['strategicSniperMode']['activated'] = False
		self.aimingSystem.XArtSniperMode = False
	return result

def new_StrategicCamera_disable(self):
	if _config_['strategicAS']['strategicSniperMode']['enabled']:
		_config_['strategicAS']['strategicSniperMode']['activated'] = False
		self.aimingSystem.XArtSniperMode = False
	return old_StrategicCamera_disable(self)

def new_StrategicCamera__cameraUpdate(self):
	result = old_StrategicCamera__cameraUpdate(self)
	if hasattr(self.aimingSystem, 'XArtSniperMode') and self.aimingSystem.XArtSniperMode and hasattr(self.aimingSystem, 'XViewMatrix'):
		cameraVectorYaw = self.aimingSystem.XViewMatrix.yaw
		cameraVectorPitch = self.aimingSystem.XViewMatrix.pitch
		self.camera.source = rotationMatrix(cameraVectorYaw, -cameraVectorPitch, 0)
		pivotShift = -rotationMatrix(0, cameraVectorPitch, 0).applyToAxis(2)
		self.camera.pivotPosition = pivotShift.scale(self._StrategicCamera__camDist) - Math.Vector3(0, self.aimingSystem.height, 0)
	elif _config_['strategicAS']['strategicSniperMode']['enabled']:
		import math
		self.camera.source = rotationMatrix(0, -math.pi * 0.499, 0)
	return result

def new_StrategicCamera_XSwitchMode(self, artSniperMode = False, shotPoint = None):
	if shotPoint is None:
		shotPoint = self.aimingSystem.getDesiredShotPoint()
	if shotPoint is not None:
		self.aimingSystem.XArtSniperMode = artSniperMode
		self.aimingSystem.updateTargetPos(shotPoint)
	return None

def new_StrategicCamera_XSetCameraBasePitch(self, cameraPitch, isDelta = False):
	shotPoint = self.aimingSystem.getDesiredShotPoint()
	if shotPoint is not None and hasattr(self.aimingSystem, 'XCameraBasePitch'):
		cameraBasePitchSrc = self.aimingSystem.XCameraBasePitch
		cameraBasePitchLimits = _config_['strategicAS']['strategicSniperMode']['cameraBasePitchLimits']
		self.aimingSystem.XCameraBasePitch = min(max(self.aimingSystem.XCameraBasePitch + cameraPitch if isDelta else cameraPitch, cameraBasePitchLimits[0]), cameraBasePitchLimits[1])
		self.aimingSystem.updateTargetPos(shotPoint)
		return (self.aimingSystem.XCameraBasePitch, self.aimingSystem.XCameraBasePitch - cameraBasePitchSrc)
	return None
