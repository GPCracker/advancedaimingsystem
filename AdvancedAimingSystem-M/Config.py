#*****
# Configuration
#*****
_config_ = None
_umlaut_ = None

#*****
# Default configuration
#*****
def defaultConfig():
	return {
		'modEnabled': ('Bool', True),
		'ignoreClientVersion': ('Bool', False),
		'hookSetTimeout': ('Float', 3.0),
		'modLoadedMessage': ('WideString', u'{} loaded.'.format(__application__)),
		'modUpdateMessage': ('WideString', u'Please update {}!'.format(__application__)),
		'commonAS': {
			'enableXRayLockForAutoAim': ('Bool', True),
			'enableXRayLockForRadialMenu': ('Bool', True),
			'enableBBoxLockForAutoAim': ('Bool', False),
			'vehicleBBoxMultiplier': ('Float', 2.0),
			'speedDisplayMultiplier': ('Float', 1.0),
			'safeShot': {
				'enabled': ('Bool', True),
				'switchKey': ('String', 'KEY_LALT'),
				'keySwitchMode': ('Bool', False),
				'activated': ('Bool', True),
				'activateMessage': ('WideString', u'SafeShot enabled.'),
				'deactivateMessage': ('WideString', u'SafeShot disabled.'),
				'shotBlockMessage': ('WideString', u'[{{shotBlockReason}}] Shot has been blocked.'),
				'shotBlockReasons': {
					'teamShot': ('WideString', u'teamShot'),
					'deadShot': ('WideString', u'deadShot'),
					'wasteShot': ('WideString', u'wasteShot')
				},
				'teamShot': {
					'blockShot': ('Bool', True),
					'blockBlueShot': ('Bool', False),
					'considerGunDirection': ('Bool', True),
					'sendChatMessage': ('Bool', True),
					'chatMessageInterval': ('Float', 5.0),
					'chatMessage': ('WideString', u'{{name}} ({{vehicle}}), you\'re in the line of my fire!')
				},
				'deadShot': {
					'blockShot': ('Bool', True),
					'blockTimeout': ('Float', 2.0)
				},
				'wasteShot': {
					'blockShot': ('Bool', False)
				}
			},
			'advancedExpertPerk': {
				'enabled': ('Bool', False),
				'cacheExtrasInfo': ('Bool', True),
				'enableQueue': ('Bool', False),
				'replyInterval': ('Float', 5.0),
				'requestInterval': ('Float', 5.0)
			}
		},
		'arcadeAS': {
			'sniperModeKeySPG': ('String', 'KEY_E'),
			'aimingInfo': {
				'enabled': ('Bool', True),
				'switchKey': ('String', 'KEY_LCONTROL+KEY_A'),
				'keySwitchMode': ('Bool', True),
				'activated': ('Bool', False),
				'bgTextureName': ('String', 'gui/maps/ingame/aim/aimingInfoBG.dds'),
				'bgAddColour': ('Vector4', Math.Vector4(0, 0, 0, 127)),
				'windowSize': ('Vector2', Math.Vector2(180, 85)),
				'windowPosition': ('Vector3', Math.Vector3(0.4, -0.1, 1.0)),
				'labelColour': ('Vector4', Math.Vector4(100, 180, 240, 255)),
				'labelTemplate': ('WideString', u'Remains: {{remainingAimingTime}}s;\nDistance: {{aimingDistance}}m;\nDeviation: {{deviation}}m;\nFly time: {{flyTime}}s;')
			}
		},
		'sniperAS': {
			'targetLock': {
				'enableXRayLock': ('Bool', True),
				'enableManualTargetLock': ('Bool', False),
				'manualTargetLockKey': ('String', 'KEY_T'),
				'enableAutoLock': ('Bool', True),
				'autoLockOnlyEnemies': ('Bool', True),
				'autoLockTargetResetTimeout': ('Float', 3.0),
				'targetInfoGUI': {
					'enabled': ('Bool', True),
					'template': ('WideString', u'Target: "{{targetShortName}}"; Speed: {{targetSpeed}}m/s.'),
					'position': ('Vector3', Math.Vector3(0, 0.30, 1.0)),
					'colour': ('Vector4', Math.Vector4(255, 127, 0, 255))
				}
			},
			'distanceLock': {
				'enableManualDistanceLock': ('Bool', True),
				'manualDistanceLockKey': ('String', 'KEY_LALT'),
				'useTargetLock': ('Bool', True),
				'distanceInfoGUI': {
					'enabled': ('Bool', True),
					'template': ('WideString', u'Distance locked: {{distance}}m.'),
					'position': ('Vector3', Math.Vector3(0, 0.25, 1.0)),
					'affectedColour': ('Vector4', Math.Vector4(0, 255, 0, 255)),
					'unaffectedColour': ('Vector4', Math.Vector4(255, 0, 0, 255))
				}
			},
			'deflectionMarker': {
				'enabled': ('Bool', False),
				'switchKey': ('String', 'KEY_LCONTROL+KEY_D'),
				'keySwitchMode': ('Bool', True),
				'activated': ('Bool', False),
				'activateMessage': ('WideString', u'Deflection calculation enabled.'),
				'deactivateMessage': ('WideString', u'Deflection calculation disabled.'),
				'textureName': ('String', 'gui/maps/ingame/aim/deflectionMarker.dds'),
				'textureSize': ('Vector2', Math.Vector2(15, 15))
			},
			'aimingInfo': {
				'enabled': ('Bool', True),
				'switchKey': ('String', 'KEY_LCONTROL+KEY_A'),
				'keySwitchMode': ('Bool', True),
				'activated': ('Bool', False),
				'bgTextureName': ('String', 'gui/maps/ingame/aim/aimingInfoBG.dds'),
				'bgAddColour': ('Vector4', Math.Vector4(0, 0, 0, 127)),
				'windowSize': ('Vector2', Math.Vector2(180, 85)),
				'windowPosition': ('Vector3', Math.Vector3(0.4, -0.25, 1.0)),
				'labelColour': ('Vector4', Math.Vector4(100, 240, 180, 255)),
				'labelTemplate': ('WideString', u'Remains: {{remainingAimingTime}}s;\nDistance: {{aimingDistance}}m;\nDeviation: {{deviation}}m;\nFly time: {{flyTime}}s;')
			}
		},
		'strategicAS': {
			'targetLock': {
				'enableXRayLock': ('Bool', True),
				'enableManualTargetLock': ('Bool', False),
				'manualTargetLockKey': ('String', 'KEY_T'),
				'enableAutoLock': ('Bool', True),
				'autoLockOnlyEnemies': ('Bool', True),
				'autoLockTargetResetTimeout': ('Float', 10.0),
				'targetInfoGUI': {
					'enabled': ('Bool', True),
					'template': ('WideString', u'Target: "{{targetShortName}}"; Speed: {{targetSpeed}}m/s.'),
					'position': ('Vector3', Math.Vector3(0, 0.30, 1.0)),
					'colour': ('Vector4', Math.Vector4(255, 127, 0, 255))
				}
			},
			'heightLock': {
				'enableManualHeightLock': ('Bool', True),
				'manualHeightLockKey': ('String', 'KEY_LALT'),
				'relativeHeightLock': {
					'enabled': ('Bool', True),
					'ignoreVehicles': ('Bool', True),
					'switchKey': ('String', 'KEY_LCONTROL+KEY_H'),
					'keySwitchMode': ('Bool', True),
					'activated': ('Bool', False),
					'targetHeightMultiplier': ('Float', 0.5),
					'activateMessage': ('WideString', u'Target height accounting enabled.'),
					'deactivateMessage': ('WideString', u'Target height accounting disabled.')
				},
				'heightInfoGUI': {
					'enabled': ('Bool', True),
					'template': ('WideString', u'Altitude locked: {{height}}m. Relative height: {{relativeHeight}}m.'),
					'position': ('Vector3', Math.Vector3(0, 0.25, 1.0)),
					'colour': ('Vector4', Math.Vector4(0, 255, 0, 255))
				}
			},
			'strategicSniperMode': {
				'enabled': ('Bool', True),
				'switchKey': ('String', 'KEY_LCONTROL+KEY_S'),
				'keySwitchMode': ('Bool', True),
				'activated': ('Bool', False),
				'cameraPitchLimits': ('Vector2', Math.Vector2(0.05, 1.57)),
				'zeroPlaneLevel': ('Float', -150.0),
				'cameraBasePitch': ('Float', 0.0),
				'cameraBasePitchLimits': ('Vector2', Math.Vector2(-0.50, 0.50)),
				'cameraBasePitchDelta': ('Float', 0.05),
				'cameraBasePitchUpKey': ('String', 'KEY_LCONTROL+KEY_R'),
				'cameraBasePitchDnKey': ('String', 'KEY_LCONTROL+KEY_F'),
				'cameraBasePitchChangeShowMessage': ('Bool', True),
				'cameraBasePitchChangeMessage': ('WideString', u'Camera base pitch >>> {{basePitch}} ({{basePitchDelta}}).'),
				'maxDistanceCorrection': ('Bool', False),
				'rangeLockEnabled': ('Bool', True),
				'rangeLockResetKey': ('String', 'KEY_SPACE')
			},
			'aimingInfo': {
				'enabled': ('Bool', True),
				'switchKey': ('String', 'KEY_LCONTROL+KEY_A'),
				'keySwitchMode': ('Bool', True),
				'activated': ('Bool', False),
				'bgTextureName': ('String', 'gui/maps/ingame/aim/aimingInfoBG.dds'),
				'bgAddColour': ('Vector4', Math.Vector4(0, 0, 0, 127)),
				'windowSize': ('Vector2', Math.Vector2(180, 85)),
				'windowPosition': ('Vector3', Math.Vector3(-0.3, -0.4, 1.0)),
				'labelColour': ('Vector4', Math.Vector4(240, 100, 100, 255)),
				'labelTemplate': ('WideString', u'Remains: {{remainingAimingTime}}s;\nDistance: {{aimingDistance}}m;\nDeviation: {{deviation}}m;\nFly time: {{flyTime}}s;')
			}
		},
		'GUIDefaultSettings': {
			'infoTextGUI': {
				'horizontalAnchor': ('String', 'CENTER'),
				'verticalAnchor': ('String', 'CENTER'),
				'heightMode': ('String', 'PIXEL'),
				'widthMode': ('String', 'PIXEL'),
				'verticalPositionMode': ('String', 'CLIP'),
				'horizontalPositionMode': ('String', 'CLIP'),
				'font': ('String', 'default_small.font')
			},
			'deflectionMarkerGUI': {
				'horizontalAnchor': ('String', 'CENTER'),
				'verticalAnchor': ('String', 'CENTER'),
				'heightMode': ('String', 'PIXEL'),
				'widthMode': ('String', 'PIXEL'),
				'horizontalPositionMode': ('String', 'CLIP'),
				'verticalPositionMode': ('String', 'CLIP'),
				'materialFX': ('String', 'BLEND')
			},
			'aimingInfoWindow': {
				'horizontalAnchor': ('String', 'CENTER'),
				'verticalAnchor': ('String', 'CENTER'),
				'heightMode': ('String', 'PIXEL'),
				'widthMode': ('String', 'PIXEL'),
				'verticalPositionMode': ('String', 'CLIP'),
				'horizontalPositionMode': ('String', 'CLIP'),
				'materialFX': ('String', 'BLEND')
			},
			'aimingInfoText': {
				'horizontalAnchor': ('String', 'LEFT'),
				'verticalAnchor': ('String', 'TOP'),
				'heightMode': ('String', 'PIXEL'),
				'widthMode': ('String', 'PIXEL'),
				'horizontalPositionMode': ('String', 'PIXEL'),
				'verticalPositionMode': ('String', 'PIXEL'),
				'font': ('String', 'default_small.font'),
				'multiline': ('Bool', True),
				'position': ('Vector3', Math.Vector3(25, 5, 1))
			}
		}
	}

#*****
# Read configuration from file
#*****
def readConfig():
	mainSection = ResMgr.openSection(__file__.replace('.pyc', '.xml'))
	if mainSection is None:
		print '[{}] Config loading failed.'.format(__appShortName__)
	else:
		print '[{}] Config successfully loaded.'.format(__appShortName__)
	return ConfigReader().readSection(mainSection, defaultConfig())

def readUmlaut():
	mainSection = ResMgr.openSection('scripts/client/mods/Umlaut.xml')
	return Umlaut(mainSection) if mainSection is not None else None
