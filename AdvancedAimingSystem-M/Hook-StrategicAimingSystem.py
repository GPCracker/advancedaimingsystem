#*****
# StrategicAimingSystem Hooks
#*****
def new_StrategicAimingSystem_handleMovement(self, dx, dy):
	if not hasattr(self, 'XArtSniperMode') or not self.XArtSniperMode or not hasattr(self, 'XViewMatrix') or not hasattr(self, 'XPlanePosition') or not hasattr(self, 'XRangeCollider') or not hasattr(self, 'XCameraBasePitch'):
		return old_StrategicAimingSystem_handleMovement(self, dx, dy)
	shiftVector = Math.Vector3(dx, 0, dy)
	desiredShotPoint = self.getDesiredShotPoint()
	if shiftVector.dot(Math.Vector3(0, 0, 1)) > 0 or desiredShotPoint is None or BigWorld.player().getOwnVehiclePosition().flatDistSqrTo(desiredShotPoint) > 1000:
		transformMatrix = rotationMatrix(self.XViewMatrix.yaw, 0, 0)
		nPlanePosition = self.XPlanePosition + transformMatrix.applyVector(shiftVector)
		hitPoint, hitVector = computeAPbyPP(nPlanePosition, [self.XRangeCollider])
		zeroPlaneLevel = _config_['strategicAS']['strategicSniperMode']['zeroPlaneLevel']
		maxDistanceCorrection = _config_['strategicAS']['strategicSniperMode']['maxDistanceCorrection']
		if not maxDistanceCorrection or computePPbyAP(hitPoint, zeroPlaneLevel)[0].flatDistSqrTo(nPlanePosition) < 1.0:
			self.XPlanePosition = nPlanePosition
			arenaBB = BoundingBox.getArenaBoundingBox()
			if not arenaBB.isPointInsideBox(hitPoint):
				aimingPoint = computeAPbyMP(arenaBB.adjustPointToBox(hitPoint))
				self.XPlanePosition = computePPbyAP(aimingPoint, zeroPlaneLevel)[0]
				hitPoint, hitVector = computeAPbyPP(self.XPlanePosition, [self.XRangeCollider])
			self.matrix.translation = hitPoint + Math.Vector3(0, self.height, 0)
			cameraPitchLimits = _config_['strategicAS']['strategicSniperMode']['cameraPitchLimits']
			self.XViewMatrix.setRotateYPR((hitVector.yaw, min(max(hitVector.pitch + self.XCameraBasePitch, cameraPitchLimits[0]), cameraPitchLimits[1]), 0))
	return None

def new_StrategicAimingSystem_updateTargetPos(self, targetPos):
	if targetPos is None or not hasattr(self, 'XArtSniperMode') or not self.XArtSniperMode or not hasattr(self, 'XViewMatrix') or not hasattr(self, 'XRangeCollider') or not hasattr(self, 'XCameraBasePitch'):
		return old_StrategicAimingSystem_updateTargetPos(self, targetPos)
	aimingPoint = computeAPbyMP(targetPos)
	zeroPlaneLevel = _config_['strategicAS']['strategicSniperMode']['zeroPlaneLevel']
	self.XPlanePosition = computePPbyAP(aimingPoint, zeroPlaneLevel)[0]
	hitPoint, hitVector = computeAPbyPP(self.XPlanePosition, externalColliders = [])
	if _config_['strategicAS']['strategicSniperMode']['rangeLockEnabled']:
		self.XRangeCollider.range = BigWorld.player().getOwnVehiclePosition().flatDistTo(hitPoint)
	self.matrix.translation = hitPoint + Math.Vector3(0, self.height, 0)
	cameraPitchLimits = _config_['strategicAS']['strategicSniperMode']['cameraPitchLimits']
	self.XViewMatrix.setRotateYPR((hitVector.yaw, min(max(hitVector.pitch, cameraPitchLimits[0]) + self.XCameraBasePitch, cameraPitchLimits[1]), 0))
	return None

def new_StrategicAimingSystem_getDesiredShotPoint(self, terrainOnlyCheck = False):
	if hasattr(self, 'XArtSniperMode') and self.XArtSniperMode:
		import AvatarInputHandler
		from AvatarInputHandler.cameras import getWorldRayAndPoint
		scanDir, scanStart = getWorldRayAndPoint(*BigWorld.player().inputHandler.ctrl.getAim().offset())
		return AvatarInputHandler.AimingSystems.getDesiredShotPoint(scanStart, scanDir, True, False)
	relativeHeightLockIgnoreVehicles = _config_['strategicAS']['heightLock']['relativeHeightLock']['ignoreVehicles']
	result = old_StrategicAimingSystem_getDesiredShotPoint(self, terrainOnlyCheck or relativeHeightLockIgnoreVehicles)
	if result is not None:
		inputHandler = BigWorld.player().inputHandler
		if hasattr(inputHandler.ctrl, 'XLockedHeight') and inputHandler.ctrl.XLockedHeight is not None:
			result.y = inputHandler.ctrl.XLockedHeight
		elif _config_['strategicAS']['heightLock']['relativeHeightLock']['enabled'] and _config_['strategicAS']['heightLock']['relativeHeightLock']['activated']:
			if hasattr(inputHandler, 'XTargetInfo') and inputHandler.XTargetInfo is not None:
				target = BigWorld.target()
				if target is None or target.id != inputHandler.XTargetInfo.id or relativeHeightLockIgnoreVehicles:
					targetHeightMultiplier = _config_['strategicAS']['heightLock']['relativeHeightLock']['targetHeightMultiplier']
					result += inputHandler.XTargetInfo.heightVector.scale(targetHeightMultiplier)
	return result
