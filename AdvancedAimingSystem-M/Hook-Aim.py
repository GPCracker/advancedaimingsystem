#*****
# ArcadeAim Hooks
#*****
def new_ArcadeAim_update(self):
	result = old_ArcadeAim_update(self)
	inputHandler = BigWorld.player().inputHandler
	currentControl = inputHandler.ctrl
	speedDisplayMultiplier = _config_['commonAS']['speedDisplayMultiplier']
	if self.mode is 'arcade':
		#Aiming info
		aimingInfoEnabled = _config_['arcadeAS']['aimingInfo']['enabled']
		aimingInfoActivated = _config_['arcadeAS']['aimingInfo']['activated']
		aimingInfoLabelTemplate = _config_['arcadeAS']['aimingInfo']['labelTemplate']
		if aimingInfoEnabled and aimingInfoActivated:
			if hasattr(currentControl, 'XAimingInfo') and currentControl.XAimingInfo is not None:
				aimingInfoLabelText = currentControl.XAimingInfo.formatTemplate(aimingInfoLabelTemplate)
				currentControl.XAimingInfo.window.label.text = _umlaut_.replace(aimingInfoLabelText) if _umlaut_ is not None else aimingInfoLabelText
	elif self.mode is 'sniper':
		#target autoLock
		enableXRayLock = _config_['sniperAS']['targetLock']['enableXRayLock']
		enableAutoLock = _config_['sniperAS']['targetLock']['enableAutoLock']
		autoLockOnlyEnemies = _config_['sniperAS']['targetLock']['autoLockOnlyEnemies']
		target = BigWorld.target()
		if target is None and enableXRayLock:
			target = XRayScanner.getTarget()
		if target is not None and TargetInfo.isVehicle(target) and target.isAlive():
			if enableAutoLock and (isVehicleEnemy(target.id) or not autoLockOnlyEnemies):
				if not hasattr(inputHandler, 'XTargetInfo') or inputHandler.XTargetInfo is None or (inputHandler.XTargetInfo.id != target.id and inputHandler.XTargetInfo.isAutoLocked):
					inputHandler.XTargetInfo = TargetInfo(target, BigWorld.time())
				elif inputHandler.XTargetInfo.isAutoLocked:
					inputHandler.XTargetInfo.lockTime = BigWorld.time()
		#target autoLock reset
		autoLockTargetResetTimeout = _config_['sniperAS']['targetLock']['autoLockTargetResetTimeout']
		if hasattr(inputHandler, 'XTargetInfo') and inputHandler.XTargetInfo is not None and inputHandler.XTargetInfo.isAutoLocked:
			if autoLockTargetResetTimeout > 0.0 and (BigWorld.time() - inputHandler.XTargetInfo.lockTime) > autoLockTargetResetTimeout:
				inputHandler.XTargetInfo = None
		#text info GUI
		distanceInfoGUITemplate = _config_['sniperAS']['distanceLock']['distanceInfoGUI']['template']
		distanceInfoGUIAColour = _config_['sniperAS']['distanceLock']['distanceInfoGUI']['affectedColour']
		distanceInfoGUIUColour = _config_['sniperAS']['distanceLock']['distanceInfoGUI']['unaffectedColour']
		targetInfoGUITemplate = _config_['sniperAS']['targetLock']['targetInfoGUI']['template']
		targetInfoGUIColour = _config_['sniperAS']['targetLock']['targetInfoGUI']['colour']
		maxShotDistance = BigWorld.player().vehicleTypeDescriptor.shot['maxDistance']
		distanceInfoGUIColour = distanceInfoGUIAColour
		distanceInfoGUIText = ''
		targetInfoGUIText = ''
		if hasattr(currentControl, 'XLockedDistance') and currentControl.XLockedDistance is not None:
			distance = currentControl.XLockedDistance
			distanceInfoGUIColour = distanceInfoGUIAColour if distance <= maxShotDistance else distanceInfoGUIUColour
			distanceInfoGUIText = formatMessage(distanceInfoGUITemplate, distance = round(distance, 1))
		if hasattr(inputHandler, 'XTargetInfo') and inputHandler.XTargetInfo is not None:
			targetShortName = inputHandler.XTargetInfo.shortName
			targetSpeed = inputHandler.XTargetInfo.speed
			targetSpeedStr = str(round(targetSpeed * speedDisplayMultiplier, 1)) if targetSpeed is not None else '--'
			targetInfoGUIText = formatMessage(targetInfoGUITemplate, targetShortName = targetShortName, targetSpeed = targetSpeedStr)
			if  _config_['sniperAS']['distanceLock']['useTargetLock']:
				cameraRay, cameraPoint = getCameraRayAndPoint()
				distance = (inputHandler.XTargetInfo.position - cameraPoint).length
				distanceInfoGUIColour = distanceInfoGUIAColour if distance <= maxShotDistance else distanceInfoGUIUColour
				distanceInfoGUIText = formatMessage(distanceInfoGUITemplate, distance = round(distance, 1))
		if hasattr(currentControl, 'XDistanceInfoGUI') and currentControl.XDistanceInfoGUI is not None:
			currentControl.XDistanceInfoGUI.gui.colour = distanceInfoGUIColour
			currentControl.XDistanceInfoGUI.gui.text = _umlaut_.replace(distanceInfoGUIText) if _umlaut_ is not None else distanceInfoGUIText
		if hasattr(currentControl, 'XTargetInfoGUI') and currentControl.XTargetInfoGUI is not None:
			currentControl.XTargetInfoGUI.gui.colour = targetInfoGUIColour
			currentControl.XTargetInfoGUI.gui.text = _umlaut_.replace(targetInfoGUIText) if _umlaut_ is not None else targetInfoGUIText
		#Deflection marker
		deflectionMarkerEnabled = _config_['sniperAS']['deflectionMarker']['enabled']
		deflectionMarkerActivated = _config_['sniperAS']['deflectionMarker']['activated']
		if deflectionMarkerEnabled and deflectionMarkerActivated:
			if hasattr(currentControl, 'XDeflectionMarker') and currentControl.XDeflectionMarker is not None and hasattr(inputHandler, 'XTargetInfo'):
				currentControl.XDeflectionMarker.updateMarker(inputHandler.XTargetInfo)
		#Aiming info
		aimingInfoEnabled = _config_['sniperAS']['aimingInfo']['enabled']
		aimingInfoActivated = _config_['sniperAS']['aimingInfo']['activated']
		aimingInfoLabelTemplate = _config_['sniperAS']['aimingInfo']['labelTemplate']
		if aimingInfoEnabled and aimingInfoActivated:
			if hasattr(currentControl, 'XAimingInfo') and currentControl.XAimingInfo is not None:
				aimingInfoLabelText = currentControl.XAimingInfo.formatTemplate(aimingInfoLabelTemplate)
				currentControl.XAimingInfo.window.label.text = _umlaut_.replace(aimingInfoLabelText) if _umlaut_ is not None else aimingInfoLabelText
	return result

#*****
# StrategicAim Hooks
#*****
def new_StrategicAim_update(self):
	result = old_StrategicAim_update(self)
	inputHandler = BigWorld.player().inputHandler
	currentControl = inputHandler.ctrl
	speedDisplayMultiplier = _config_['commonAS']['speedDisplayMultiplier']
	if self.mode is 'strategic':
		#target autoLock
		enableXRayLock = _config_['strategicAS']['targetLock']['enableXRayLock']
		enableAutoLock = _config_['strategicAS']['targetLock']['enableAutoLock']
		autoLockOnlyEnemies = _config_['strategicAS']['targetLock']['autoLockOnlyEnemies']
		target = BigWorld.target()
		if target is None and enableXRayLock:
			target = XRayScanner.getTarget()
		if target is not None and TargetInfo.isVehicle(target) and target.isAlive():
			if enableAutoLock and (isVehicleEnemy(target.id) or not autoLockOnlyEnemies):
				if not hasattr(inputHandler, 'XTargetInfo') or inputHandler.XTargetInfo is None or (inputHandler.XTargetInfo.id != target.id and inputHandler.XTargetInfo.isAutoLocked):
					inputHandler.XTargetInfo = TargetInfo(target, BigWorld.time())
				elif inputHandler.XTargetInfo.isAutoLocked:
					inputHandler.XTargetInfo.lockTime = BigWorld.time()
		#target autoLock reset
		autoLockTargetResetTimeout = _config_['strategicAS']['targetLock']['autoLockTargetResetTimeout']
		if hasattr(inputHandler, 'XTargetInfo') and inputHandler.XTargetInfo is not None and inputHandler.XTargetInfo.isAutoLocked:
			if autoLockTargetResetTimeout > 0.0 and (BigWorld.time() - inputHandler.XTargetInfo.lockTime) > autoLockTargetResetTimeout:
				inputHandler.XTargetInfo = None
		#text info GUI
		heightInfoGUITemplate = _config_['strategicAS']['heightLock']['heightInfoGUI']['template']
		heightInfoGUIColour = _config_['strategicAS']['heightLock']['heightInfoGUI']['colour']
		targetInfoGUITemplate = _config_['strategicAS']['targetLock']['targetInfoGUI']['template']
		targetInfoGUIColour = _config_['strategicAS']['targetLock']['targetInfoGUI']['colour']
		heightInfoGUIText = ''
		targetInfoGUIText = ''
		if hasattr(currentControl, 'XLockedHeight') and currentControl.XLockedHeight is not None:
			playerPosition = BigWorld.player().getOwnVehiclePosition()
			height = currentControl.XLockedHeight
			relativeHeight = height - playerPosition.y
			heightInfoGUIText = formatMessage(heightInfoGUITemplate, height = round(height, 1), relativeHeight = round(relativeHeight, 1))
		if hasattr(inputHandler, 'XTargetInfo') and inputHandler.XTargetInfo is not None:
			targetShortName = inputHandler.XTargetInfo.shortName
			targetSpeed = inputHandler.XTargetInfo.speed
			targetSpeedStr = str(round(targetSpeed * speedDisplayMultiplier, 1)) if targetSpeed is not None else '--'
			playerPosition = BigWorld.player().getOwnVehiclePosition()
			targetPosition = inputHandler.XTargetInfo.position
			height = targetPosition.y
			relativeHeight = (targetPosition - playerPosition).y
			targetInfoGUIText = formatMessage(targetInfoGUITemplate, targetShortName = targetShortName, targetSpeed = targetSpeedStr)
			heightInfoGUIText = formatMessage(heightInfoGUITemplate, height = round(height, 1), relativeHeight = round(relativeHeight, 1))
		if hasattr(currentControl, 'XHeightInfoGUI') and currentControl.XHeightInfoGUI is not None:
			currentControl.XHeightInfoGUI.gui.colour = heightInfoGUIColour
			currentControl.XHeightInfoGUI.gui.text = _umlaut_.replace(heightInfoGUIText) if _umlaut_ is not None else heightInfoGUIText
		if hasattr(currentControl, 'XTargetInfoGUI') and currentControl.XTargetInfoGUI is not None:
			currentControl.XTargetInfoGUI.gui.colour = targetInfoGUIColour
			currentControl.XTargetInfoGUI.gui.text = _umlaut_.replace(targetInfoGUIText) if _umlaut_ is not None else targetInfoGUIText
		#Aiming info
		aimingInfoEnabled = _config_['strategicAS']['aimingInfo']['enabled']
		aimingInfoActivated = _config_['strategicAS']['aimingInfo']['activated']
		aimingInfoLabelTemplate = _config_['strategicAS']['aimingInfo']['labelTemplate']
		if aimingInfoEnabled and aimingInfoActivated:
			if hasattr(currentControl, 'XAimingInfo') and currentControl.XAimingInfo is not None:
				aimingInfoLabelText = currentControl.XAimingInfo.formatTemplate(aimingInfoLabelTemplate)
				currentControl.XAimingInfo.window.label.text = _umlaut_.replace(aimingInfoLabelText) if _umlaut_ is not None else aimingInfoLabelText
	return result
