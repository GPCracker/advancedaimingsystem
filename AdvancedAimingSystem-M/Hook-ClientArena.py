#*****
# ClientArena Hooks
#*****
def new_ClientArena_collideWithSpaceBB(self, start, end):
	inputHandler = BigWorld.player().inputHandler
	currentControl = inputHandler.ctrl
	cameraRay, cameraPoint = getCameraRayAndPoint()
	distance = None
	if hasattr(currentControl, 'XLockedDistance') and currentControl.XLockedDistance is not None:
		distance = currentControl.XLockedDistance
	elif inputHandler.aim.mode is 'sniper' and _config_['sniperAS']['distanceLock']['useTargetLock']:
		if hasattr(inputHandler, 'XTargetInfo') and inputHandler.XTargetInfo is not None:
			targetPosition = inputHandler.XTargetInfo.position
			if targetPosition is not None:
				distance = (targetPosition - cameraPoint).length
	if distance is not None:
		csV = start - cameraPoint
		ceV = end - cameraPoint
		seV = end - start
		if csV.length <= distance and ceV.length >= distance:
			#this interval of collision check holds required marker point
			return start + seV * (distance - csV.length) / seV.length
	return old_ClientArena_collideWithSpaceBB(self, start, end)

def new_ClientArena__onVehicleKilled(self, argStr):
	result = old_ClientArena__onVehicleKilled(self, argStr)
	import cPickle
	victimID, killerID, reason = cPickle.loads(argStr)
	inputHandler = BigWorld.player().inputHandler
	if hasattr(inputHandler, 'XTargetInfo') and inputHandler.XTargetInfo is not None:
		if victimID == inputHandler.XTargetInfo.id:
			#locked target has been killed - unlock
			inputHandler.XTargetInfo = None
	if not hasattr(self, 'XVehiclesDeathTime') or self.XVehiclesDeathTime is None:
		self.XVehiclesDeathTime = {}
	if hasattr(self, 'XVehiclesDeathTime') and self.XVehiclesDeathTime is not None:
		self.XVehiclesDeathTime[victimID] = BigWorld.time()
	return result
