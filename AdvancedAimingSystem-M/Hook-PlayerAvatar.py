#*****
# PlayerAvatar Hooks
#*****
def new_PlayerAvatar_shoot(self, isRepeat = False):
	safeShotEnabled = _config_['commonAS']['safeShot']['enabled']
	safeShotActivated = _config_['commonAS']['safeShot']['activated']
	shotBlockMessage = _config_['commonAS']['safeShot']['shotBlockMessage']
	blockTeamShot = _config_['commonAS']['safeShot']['teamShot']['blockShot']
	blockBlueShot = _config_['commonAS']['safeShot']['teamShot']['blockBlueShot']
	sendChatMessage = _config_['commonAS']['safeShot']['teamShot']['sendChatMessage']
	chatMessageInterval = _config_['commonAS']['safeShot']['teamShot']['chatMessageInterval']
	chatMessage = _config_['commonAS']['safeShot']['teamShot']['chatMessage']
	blockDeadShot = _config_['commonAS']['safeShot']['deadShot']['blockShot']
	blockTimeout = _config_['commonAS']['safeShot']['deadShot']['blockTimeout']
	blockWasteShot = _config_['commonAS']['safeShot']['wasteShot']['blockShot']
	considerGunDirection = _config_['commonAS']['safeShot']['teamShot']['considerGunDirection']
	if not safeShotEnabled or not safeShotActivated:
		return old_PlayerAvatar_shoot(self, isRepeat)
	vehiclesDeathTime = self.arena.XVehiclesDeathTime if hasattr(self.arena, 'XVehiclesDeathTime') and self.arena.XVehiclesDeathTime is not None else {}
	shotBlockReason = None
	target = BigWorld.target()
	entityCollisionData = getGunMarkerData()[3]
	import Vehicle
	aimTarget = target if isinstance(target, Vehicle.Vehicle) else None
	gunTarget = entityCollisionData.entity if entityCollisionData is not None and isinstance(entityCollisionData.entity, Vehicle.Vehicle) else None
	if blockWasteShot and BigWorld.player().inputHandler.aim.mode is 'arcade' and gunTarget is None:
		shotBlockReason = 'wasteShot'
		shotTarget = gunTarget
	elif blockTeamShot and considerGunDirection and gunTarget is not None and isVehicleAlly(gunTarget.id) and isVehicleAlive(gunTarget.id) and (blockBlueShot or not isVehicleTeamKiller(gunTarget.id)):
		shotBlockReason = 'teamShot'
		shotTarget = gunTarget
	elif blockTeamShot and aimTarget is not None and isVehicleAlly(aimTarget.id) and isVehicleAlive(aimTarget.id) and (blockBlueShot or not isVehicleTeamKiller(aimTarget.id)):
		shotBlockReason = 'teamShot'
		shotTarget = aimTarget
	elif blockDeadShot and aimTarget is not None and isVehicleEnemy(aimTarget.id) and not isVehicleAlive(aimTarget.id) and aimTarget.id in vehiclesDeathTime and vehiclesDeathTime[aimTarget.id] + blockTimeout > BigWorld.time():
		shotBlockReason = 'deadShot'
		shotTarget = aimTarget
	if shotBlockReason is None:
		return old_PlayerAvatar_shoot(self, isRepeat)
	shotBlockReasonText = _config_['commonAS']['safeShot']['shotBlockReasons'][shotBlockReason]
	showMessageOnPanel('VehicleErrorsPanel', shotBlockReason, formatMessage(shotBlockMessage, shotBlockReason = shotBlockReasonText), 'red')
	if shotBlockReason is 'teamShot' and sendChatMessage:
		teamChannel = getBattleChatControllers()['team']
		if teamChannel is not None and (not hasattr(self, 'XSafeShotLastMessageVehicleID') or self.XSafeShotLastMessageVehicleID != shotTarget.id or not hasattr(self, 'XSafeShotLastMessageTime') or self.XSafeShotLastMessageTime + chatMessageInterval < BigWorld.time()):
			self.XSafeShotLastMessageVehicleID = shotTarget.id
			self.XSafeShotLastMessageTime = BigWorld.time()
			playerName = self.arena.vehicles[shotTarget.id]['name']
			playerVehicle = self.arena.vehicles[shotTarget.id]['vehicleType'].type.shortUserString
			teamChannel.sendMessage(formatMessage(chatMessage.encode('utf-8'), name = playerName, vehicle = playerVehicle))
	if self._PlayerAvatar__tryShootCallbackId is None:
		self._PlayerAvatar__tryShootCallbackId = BigWorld.callback(0.0, self._PlayerAvatar__tryShootCallback)
	return None

def new_PlayerAvatar_updateVehicleMiscStatus(self, vehicleID, code, intArg, floatArg):
	result = old_PlayerAvatar_updateVehicleMiscStatus(self, vehicleID, code, intArg, floatArg)
	import constants
	if _config_['commonAS']['advancedExpertPerk']['enabled'] and vehicleID == self.playerVehicleID and code == constants.VEHICLE_MISC_STATUS.OTHER_VEHICLE_DAMAGED_DEVICES_VISIBLE:
		self.XMaySeeOtherVehicleDamagedDevices = bool(intArg)
		if self.XMaySeeOtherVehicleDamagedDevices and (not hasattr(self, 'XAdvancedExpertPerk') or self.XAdvancedExpertPerk is None):
			expertStatusFunc = lambda: BigWorld.player().XMaySeeOtherVehicleDamagedDevices
			cacheExtrasInfo = _config_['commonAS']['advancedExpertPerk']['cacheExtrasInfo']
			enableQueue = _config_['commonAS']['advancedExpertPerk']['enableQueue']
			replyInterval = _config_['commonAS']['advancedExpertPerk']['replyInterval']
			requestInterval = _config_['commonAS']['advancedExpertPerk']['requestInterval']
			self.XAdvancedExpertPerk = AdvancedExpertPerk(expertStatusFunc, cacheExtrasInfo, enableQueue, replyInterval, requestInterval)
	return result

def new_PlayerAvatar_showOtherVehicleDamagedDevices(self, vehicleID, damagedExtras, destroyedExtras):
	result = old_PlayerAvatar_showOtherVehicleDamagedDevices(self, vehicleID, damagedExtras, destroyedExtras)
	if _config_['commonAS']['advancedExpertPerk']['enabled']:
		if hasattr(self, 'XAdvancedExpertPerk') and self.XAdvancedExpertPerk is not None:
			self.XAdvancedExpertPerk.onExtrasInfoReceived(vehicleID, damagedExtras, destroyedExtras)
		import Vehicle
		target = BigWorld.target()
		if target is not None and isinstance(target, Vehicle.Vehicle) and target.id == vehicleID:
			import gui.WindowsManager
			gui.WindowsManager.g_windowsManager.battleWindow.damageInfoPanel.show(vehicleID, damagedExtras, destroyedExtras)
	return result

def new_PlayerAvatar_targetBlur(self, prevEntity):
	result = old_PlayerAvatar_targetBlur(self, prevEntity)
	if _config_['commonAS']['advancedExpertPerk']['enabled']:
		import Vehicle
		if isinstance(prevEntity, Vehicle.Vehicle) and hasattr(self, 'XMaySeeOtherVehicleDamagedDevices') and self.XMaySeeOtherVehicleDamagedDevices:
			import gui.WindowsManager
			gui.WindowsManager.g_windowsManager.battleWindow.damageInfoPanel.hide()
	return result

def new_PlayerAvatar_targetFocus(self, entity):
	result = old_PlayerAvatar_targetFocus(self, entity)
	if _config_['commonAS']['advancedExpertPerk']['enabled']:
		import Vehicle
		if isinstance(entity, Vehicle.Vehicle) and entity.publicInfo['team'] is not self.team and entity.isAlive():
			if hasattr(self, 'XAdvancedExpertPerk') and self.XAdvancedExpertPerk is not None:
				self.XAdvancedExpertPerk.requestExtrasInfo(entity.id)
				cachedData = self.XAdvancedExpertPerk.getCachedExtrasInfo(entity.id)
				if cachedData is not None:
					damagedExtras, destroyedExtras = cachedData
					import gui.WindowsManager
					gui.WindowsManager.g_windowsManager.battleWindow.damageInfoPanel.show(entity.id, damagedExtras, destroyedExtras)
	return result

def new_PlayerAvatar_autoAim(self, target):
	if _config_['commonAS']['advancedExpertPerk']['enabled']:
		import Vehicle
		if target is not None and isinstance(target, Vehicle.Vehicle) and target.publicInfo['team'] is not self.team and target.isAlive():
			if hasattr(self, 'XAdvancedExpertPerk') and self.XAdvancedExpertPerk is not None:
				self.XAdvancedExpertPerk.requestExtrasInfo(target.id)
	if target is None and BigWorld.target() is None and _config_['commonAS']['enableXRayLockForAutoAim'] and self.inputHandler.aim.mode is not 'strategic':
		target = XRayScanner.getTarget()
	if target is None and BigWorld.target() is None and _config_['commonAS']['enableBBoxLockForAutoAim'] and self.inputHandler.aim.mode is not 'strategic':
		vehiclesFilter = lambda vehicleID: isVehicleEnemy(vehicleID) and isVehicleAlive(vehicleID)
		target = BoundingBoxScanner.getTarget(scalar=_config_['commonAS']['vehicleBBoxMultiplier'], fltr=vehiclesFilter)
	return old_PlayerAvatar_autoAim(self, target)

def new_PlayerAvatar_MSOVDD_getter(self):
	return not _config_['commonAS']['advancedExpertPerk']['enabled'] and self._maySeeOtherVehicleDamagedDevices

def new_PlayerAvatar_MSOVDD_setter(self, value):
	self._maySeeOtherVehicleDamagedDevices = value
	return None
