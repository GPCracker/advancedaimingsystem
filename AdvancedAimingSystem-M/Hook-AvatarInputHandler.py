#*****
# AvatarInputHandler Hooks
#*****
def new_AvatarInputHandler_handleKeyEvent(self, event):
	result = old_AvatarInputHandler_handleKeyEvent(self, event)
	key, isDown, isRepeat, mods = keyEventParse(event)
	aimMode = self.ctrl.getAim().mode
	if aimMode in ['arcade', 'sniper', 'strategic']:
		safeShotEnabled = _config_['commonAS']['safeShot']['enabled']
		safeShotKey = _config_['commonAS']['safeShot']['switchKey']
		safeShotKeySwitchMode = _config_['commonAS']['safeShot']['keySwitchMode']
		if (key, mods) == parseHotKeySequence(safeShotKey) and not isRepeat and safeShotEnabled:
			if safeShotKeySwitchMode and isDown:
				_config_['commonAS']['safeShot']['activated'] = not _config_['commonAS']['safeShot']['activated']
				if _config_['commonAS']['safeShot']['activated']:
					showMessageOnPanel('PlayerMessagesPanel', 0, _config_['commonAS']['safeShot']['activateMessage'], 'green')
				else:
					showMessageOnPanel('PlayerMessagesPanel', 0, _config_['commonAS']['safeShot']['deactivateMessage'], 'red')
			elif not safeShotKeySwitchMode:
				_config_['commonAS']['safeShot']['activated'] = not isDown
	if aimMode is 'arcade':
		sniperModeKeySPG = _config_['arcadeAS']['sniperModeKeySPG']
		vehicleClass = getVehicleClass(BigWorld.player().playerVehicleID)
		aimingInfoEnabled = _config_['arcadeAS']['aimingInfo']['enabled']
		aimingInfoSwitchKey = _config_['arcadeAS']['aimingInfo']['switchKey']
		aimingInfoKeySwitchMode = _config_['arcadeAS']['aimingInfo']['keySwitchMode']
		if (key, mods) == parseHotKeySequence(sniperModeKeySPG) and isDown and not isRepeat and vehicleClass is 'SPG':
			desiredShotPoint = self.ctrl.camera.aimingSystem.getDesiredShotPoint()
			self.onControlModeChanged('sniper', preferredPos=desiredShotPoint, aimingMode=self.ctrl._aimingMode, saveZoom=True)
		elif (key, mods) == parseHotKeySequence(aimingInfoSwitchKey) and not isRepeat and aimingInfoEnabled:
			if aimingInfoKeySwitchMode and isDown:
				_config_['arcadeAS']['aimingInfo']['activated'] = not _config_['arcadeAS']['aimingInfo']['activated']
			elif not aimingInfoKeySwitchMode:
				_config_['arcadeAS']['aimingInfo']['activated'] = isDown
			if hasattr(self.ctrl, 'XAimingInfo') and self.ctrl.XAimingInfo is not None:
				self.ctrl.XAimingInfo.window.visible = _config_['arcadeAS']['aimingInfo']['activated']
	elif aimMode is 'sniper':
		vehicleClass = getVehicleClass(BigWorld.player().playerVehicleID)
		enableManualDistanceLock = _config_['sniperAS']['distanceLock']['enableManualDistanceLock']
		manualDistanceLockKey = _config_['sniperAS']['distanceLock']['manualDistanceLockKey']
		enableManualTargetLock = _config_['sniperAS']['targetLock']['enableManualTargetLock']
		manualTargetLockKey = _config_['sniperAS']['targetLock']['manualTargetLockKey']
		enableXRayLock = _config_['sniperAS']['targetLock']['enableXRayLock']
		sniperModeKeySPG = _config_['arcadeAS']['sniperModeKeySPG']
		deflectionMarkerEnabled = _config_['sniperAS']['deflectionMarker']['enabled']
		deflectionMarkerSwitchKey = _config_['sniperAS']['deflectionMarker']['switchKey']
		deflectionMarkerKeySwitchMode = _config_['sniperAS']['deflectionMarker']['keySwitchMode']
		aimingInfoEnabled = _config_['sniperAS']['aimingInfo']['enabled']
		aimingInfoSwitchKey = _config_['sniperAS']['aimingInfo']['switchKey']
		aimingInfoKeySwitchMode = _config_['sniperAS']['aimingInfo']['keySwitchMode']
		if (key, mods) == parseHotKeySequence(manualDistanceLockKey) and not isRepeat and enableManualDistanceLock:
			#unlock distance
			self.ctrl.XLockedDistance = None
			#stop tracking target
			self.XTargetInfo = None
			if isDown:
				cameraRay, cameraPoint = getCameraRayAndPoint()
				shotPoint = self.ctrl.getDesiredShotPoint()
				if shotPoint is not None:
					#lock distance
					self.ctrl.XLockedDistance = (shotPoint - cameraPoint).length
		elif (key, mods) == parseHotKeySequence(manualTargetLockKey) and isDown and not isRepeat and enableManualTargetLock:
			if not hasattr(self.ctrl, 'XLockedDistance') or self.ctrl.XLockedDistance is None:
				#distance is not locked
				target = BigWorld.target()
				if enableXRayLock and target is None:
					target = XRayScanner.getTarget()
				if target is not None:
					#set target for tracking
					self.XTargetInfo = TargetInfo(target)
				else:
					#reset target
					self.XTargetInfo = None
		elif (key, mods) == parseHotKeySequence(sniperModeKeySPG) and isDown and not isRepeat and vehicleClass is 'SPG':
			desiredShotPoint = self.ctrl.camera.aimingSystem.getDesiredShotPoint()
			self.onControlModeChanged('arcade', preferredPos=desiredShotPoint, turretYaw=self.ctrl.camera.aimingSystem.turretYaw, gunPitch=self.ctrl.camera.aimingSystem.gunPitch, aimingMode=self.ctrl._aimingMode)
		elif (key, mods) == parseHotKeySequence(deflectionMarkerSwitchKey) and not isRepeat and deflectionMarkerEnabled:
			if deflectionMarkerKeySwitchMode and isDown:
				_config_['sniperAS']['deflectionMarker']['activated'] = not _config_['sniperAS']['deflectionMarker']['activated']
				if _config_['sniperAS']['deflectionMarker']['activated']:
					showMessageOnPanel('PlayerMessagesPanel', 0, _config_['sniperAS']['deflectionMarker']['activateMessage'], 'green')
				else:
					showMessageOnPanel('PlayerMessagesPanel', 0, _config_['sniperAS']['deflectionMarker']['deactivateMessage'], 'red')
			elif not deflectionMarkerKeySwitchMode:
				_config_['sniperAS']['deflectionMarker']['activated'] = isDown
			if hasattr(self, 'XDeflectionMarker') and self.XDeflectionMarker is not None:
				self.XDeflectionMarker.display = _config_['sniperAS']['deflectionMarker']['activated']
		elif (key, mods) == parseHotKeySequence(aimingInfoSwitchKey) and not isRepeat and aimingInfoEnabled:
			if aimingInfoKeySwitchMode and isDown:
				_config_['sniperAS']['aimingInfo']['activated'] = not _config_['sniperAS']['aimingInfo']['activated']
			elif not aimingInfoKeySwitchMode:
				_config_['sniperAS']['aimingInfo']['activated'] = isDown
			if hasattr(self.ctrl, 'XAimingInfo') and self.ctrl.XAimingInfo is not None:
				self.ctrl.XAimingInfo.window.visible = _config_['sniperAS']['aimingInfo']['activated']
	elif aimMode is 'strategic':
		enableManualHeightLock = _config_['strategicAS']['heightLock']['enableManualHeightLock']
		manualHeightLockKey = _config_['strategicAS']['heightLock']['manualHeightLockKey']
		relativeHeightLockEnabled = _config_['strategicAS']['heightLock']['relativeHeightLock']['enabled']
		relativeHeightLockSwitchKey = _config_['strategicAS']['heightLock']['relativeHeightLock']['switchKey']
		relativeHeightLockKeySwitchMode = _config_['strategicAS']['heightLock']['relativeHeightLock']['keySwitchMode']
		enableManualTargetLock = _config_['strategicAS']['targetLock']['enableManualTargetLock']
		manualTargetLockKey = _config_['strategicAS']['targetLock']['manualTargetLockKey']
		enableXRayLock = _config_['strategicAS']['targetLock']['enableXRayLock']
		strategicSniperModeEnabled = _config_['strategicAS']['strategicSniperMode']['enabled']
		strategicSniperModeSwitchKey = _config_['strategicAS']['strategicSniperMode']['switchKey']
		strategicSniperModeKeySwitchMode = _config_['strategicAS']['strategicSniperMode']['keySwitchMode']
		strategicSniperModeCameraBasePitchDelta = _config_['strategicAS']['strategicSniperMode']['cameraBasePitchDelta']
		strategicSniperModeCameraBasePitchUpKey = _config_['strategicAS']['strategicSniperMode']['cameraBasePitchUpKey']
		strategicSniperModeCameraBasePitchDnKey = _config_['strategicAS']['strategicSniperMode']['cameraBasePitchDnKey']
		strategicSniperModeRangeLockEnabled = _config_['strategicAS']['strategicSniperMode']['rangeLockEnabled']
		strategicSniperModeRangeLockResetKey = _config_['strategicAS']['strategicSniperMode']['rangeLockResetKey']
		aimingInfoEnabled = _config_['strategicAS']['aimingInfo']['enabled']
		aimingInfoSwitchKey = _config_['strategicAS']['aimingInfo']['switchKey']
		aimingInfoKeySwitchMode = _config_['strategicAS']['aimingInfo']['keySwitchMode']
		if (key, mods) == parseHotKeySequence(manualHeightLockKey)  and not isRepeat and enableManualHeightLock:
			#unlock height
			self.ctrl.XLockedHeight = None
			#stop tracking target
			self.XTargetInfo = None
			if isDown:
				#lock height
				shotPoint = self.getDesiredShotPoint()
				if shotPoint is not None:
					self.ctrl.XLockedHeight = shotPoint.y
		elif (key, mods) == parseHotKeySequence(relativeHeightLockSwitchKey) and not isRepeat and relativeHeightLockEnabled:
			if relativeHeightLockKeySwitchMode and isDown:
				_config_['strategicAS']['heightLock']['relativeHeightLock']['activated'] = not _config_['strategicAS']['heightLock']['relativeHeightLock']['activated']
				if _config_['strategicAS']['heightLock']['relativeHeightLock']['activated']:
					showMessageOnPanel('PlayerMessagesPanel', 0, _config_['strategicAS']['heightLock']['relativeHeightLock']['activateMessage'], 'green')
				else:
					showMessageOnPanel('PlayerMessagesPanel', 0, _config_['strategicAS']['heightLock']['relativeHeightLock']['deactivateMessage'], 'red')
			elif not relativeHeightLockKeySwitchMode:
				_config_['strategicAS']['heightLock']['relativeHeightLock']['activated'] = isDown
		elif (key, mods) == parseHotKeySequence(manualTargetLockKey) and isDown and not isRepeat and enableManualTargetLock:
			if not hasattr(self.ctrl, 'XLockedHeight') or self.ctrl.XLockedHeight is None:
				#height is not locked
				target = BigWorld.target()
				if enableXRayLock and target is None:
					target = XRayScanner.getTarget()
				if target is not None:
					#set target for tracking
					self.XTargetInfo = TargetInfo(target)
				else:
					#reset target
					self.XTargetInfo = None
		elif (key, mods) == parseHotKeySequence(strategicSniperModeSwitchKey) and not isRepeat and strategicSniperModeEnabled:
			if strategicSniperModeKeySwitchMode and isDown:
				_config_['strategicAS']['strategicSniperMode']['activated'] = not _config_['strategicAS']['strategicSniperMode']['activated']
			elif not strategicSniperModeKeySwitchMode:
				_config_['strategicAS']['strategicSniperMode']['activated'] = isDown
			self.ctrl.camera.XSwitchMode(_config_['strategicAS']['strategicSniperMode']['activated'])
		elif (key, mods) == parseHotKeySequence(strategicSniperModeCameraBasePitchUpKey) and isDown and not isRepeat and strategicSniperModeEnabled:
			cameraBasePitchChangeResult = self.ctrl.camera.XSetCameraBasePitch(+strategicSniperModeCameraBasePitchDelta, True)
			if cameraBasePitchChangeResult is not None and cameraBasePitchChangeResult[1]:
				_config_['strategicAS']['strategicSniperMode']['cameraBasePitch'] = cameraBasePitchChangeResult[0]
				if _config_['strategicAS']['strategicSniperMode']['cameraBasePitchChangeShowMessage']:
					cameraBasePitchChangeMessage = formatMessage(_config_['strategicAS']['strategicSniperMode']['cameraBasePitchChangeMessage'], basePitch = round(cameraBasePitchChangeResult[0], 2), basePitchDelta = round(cameraBasePitchChangeResult[1], 2))
					showMessageOnPanel('PlayerMessagesPanel', 0, cameraBasePitchChangeMessage, 'green')
		elif (key, mods) == parseHotKeySequence(strategicSniperModeCameraBasePitchDnKey) and isDown and not isRepeat and strategicSniperModeEnabled:
			cameraBasePitchChangeResult = self.ctrl.camera.XSetCameraBasePitch(-strategicSniperModeCameraBasePitchDelta, True)
			if cameraBasePitchChangeResult is not None and cameraBasePitchChangeResult[1]:
				_config_['strategicAS']['strategicSniperMode']['cameraBasePitch'] = cameraBasePitchChangeResult[0]
				if _config_['strategicAS']['strategicSniperMode']['cameraBasePitchChangeShowMessage']:
					cameraBasePitchChangeMessage = formatMessage(_config_['strategicAS']['strategicSniperMode']['cameraBasePitchChangeMessage'], basePitch = round(cameraBasePitchChangeResult[0], 2), basePitchDelta = round(cameraBasePitchChangeResult[1], 2))
					showMessageOnPanel('PlayerMessagesPanel', 0, cameraBasePitchChangeMessage, 'red')
		elif (key, mods) == parseHotKeySequence(strategicSniperModeRangeLockResetKey) and isDown and not isRepeat and strategicSniperModeRangeLockEnabled:
			self.ctrl.camera.aimingSystem.XRangeCollider.resetRange()
		elif (key, mods) == parseHotKeySequence(aimingInfoSwitchKey) and not isRepeat and aimingInfoEnabled:
			if aimingInfoKeySwitchMode and isDown:
				_config_['strategicAS']['aimingInfo']['activated'] = not _config_['strategicAS']['aimingInfo']['activated']
			elif not aimingInfoKeySwitchMode:
				_config_['strategicAS']['aimingInfo']['activated'] = isDown
			if hasattr(self.ctrl, 'XAimingInfo') and self.ctrl.XAimingInfo is not None:
				self.ctrl.XAimingInfo.window.visible = _config_['strategicAS']['aimingInfo']['activated']
	return result
