#*****
# SniperControlMode Hooks
#*****
def new_SniperControlMode_enable(self, **args):
	result = old_SniperControlMode_enable(self, **args)
	#unlock distance
	self.XLockedDistance = None
	#text info GUI
	if _config_['sniperAS']['distanceLock']['distanceInfoGUI']['enabled']:
		if not hasattr(self, 'XDistanceInfoGUI') or self.XDistanceInfoGUI is None:
			#init GUI
			self.XDistanceInfoGUI = GUIWrapper.TextGUI(_config_['GUIDefaultSettings']['infoTextGUI'])
			self.XDistanceInfoGUI.gui.position = _config_['sniperAS']['distanceLock']['distanceInfoGUI']['position']
			self.XDistanceInfoGUI.gui.colour = _config_['sniperAS']['distanceLock']['distanceInfoGUI']['affectedColour']
			self.XDistanceInfoGUI.append()
		if hasattr(self, 'XDistanceInfoGUI') and self.XDistanceInfoGUI is not None:
			#show GUI
			self.XDistanceInfoGUI.gui.visible = True
	if _config_['sniperAS']['targetLock']['targetInfoGUI']['enabled']:
		if not hasattr(self, 'XTargetInfoGUI') or self.XTargetInfoGUI is None:
			#init GUI
			self.XTargetInfoGUI = GUIWrapper.TextGUI(_config_['GUIDefaultSettings']['infoTextGUI'])
			self.XTargetInfoGUI.gui.position = _config_['sniperAS']['targetLock']['targetInfoGUI']['position']
			self.XTargetInfoGUI.gui.colour = _config_['sniperAS']['targetLock']['targetInfoGUI']['colour']
			self.XTargetInfoGUI.append()
		if hasattr(self, 'XTargetInfoGUI') and self.XTargetInfoGUI is not None:
			#show GUI
			self.XTargetInfoGUI.gui.visible = True
	#Deflection marker
	if _config_['sniperAS']['deflectionMarker']['enabled']:
		if not hasattr(self, 'XDeflectionMarker') or self.XDeflectionMarker is None:
			#init Deflection marker
			self.XDeflectionMarker = DeflectionMarker(_config_['GUIDefaultSettings']['deflectionMarkerGUI'])
			self.XDeflectionMarker.marker.textureName = _config_['sniperAS']['deflectionMarker']['textureName']
			self.XDeflectionMarker.marker.size = _config_['sniperAS']['deflectionMarker']['textureSize']
		if hasattr(self, 'XDeflectionMarker') and self.XDeflectionMarker is not None:
			self.XDeflectionMarker.display = _config_['sniperAS']['deflectionMarker']['activated']
	#Aiming info GUI
	if _config_['sniperAS']['aimingInfo']['enabled']:
		if not hasattr(self, 'XAimingInfo') or self.XAimingInfo is None:
			#init GUI
			self.XAimingInfo = AimingInfo(_config_['GUIDefaultSettings']['aimingInfoWindow'], _config_['GUIDefaultSettings']['aimingInfoText'])
			self.XAimingInfo.window.textureName = _config_['sniperAS']['aimingInfo']['bgTextureName']
			self.XAimingInfo.window.colour = _config_['sniperAS']['aimingInfo']['bgAddColour']
			self.XAimingInfo.window.size = _config_['sniperAS']['aimingInfo']['windowSize']
			self.XAimingInfo.window.position = _config_['sniperAS']['aimingInfo']['windowPosition']
			self.XAimingInfo.window.label.colour = _config_['sniperAS']['aimingInfo']['labelColour']
		if hasattr(self, 'XAimingInfo') and self.XAimingInfo is not None:
			#show GUI
			self.XAimingInfo.window.visible = _config_['sniperAS']['aimingInfo']['activated']
	return result

def new_SniperControlMode_disable(self, isDestroy = False):
	result = old_SniperControlMode_disable(self, isDestroy)
	#unlock distance
	self.XLockedDistance = None
	#text info GUI
	if _config_['sniperAS']['distanceLock']['distanceInfoGUI']['enabled']:
		if hasattr(self, 'XDistanceInfoGUI') and self.XDistanceInfoGUI is not None:
			#hide GUI
			self.XDistanceInfoGUI.gui.visible = False
	if _config_['sniperAS']['targetLock']['targetInfoGUI']['enabled']:
		if hasattr(self, 'XTargetInfoGUI') and self.XTargetInfoGUI is not None:
			#hide GUI
			self.XTargetInfoGUI.gui.visible = False
	#Deflection marker
	if _config_['sniperAS']['deflectionMarker']['enabled']:
		if hasattr(self, 'XDeflectionMarker') and self.XDeflectionMarker is not None:
			self.XDeflectionMarker.display = False
	#Aiming info GUI
	if _config_['sniperAS']['aimingInfo']['enabled']:
		if hasattr(self, 'XAimingInfo') and self.XAimingInfo is not None:
			#hide GUI
			self.XAimingInfo.window.visible = False
	return result

def new_SniperControlMode_getDesiredShotPoint(self):
	result = old_SniperControlMode_getDesiredShotPoint(self)
	if result is not None:
		from AvatarInputHandler.cameras import getWorldRayAndPoint
		cameraRay, cameraPoint = getWorldRayAndPoint(*self.getAim().offset())
		cameraRay.normalise()
		if hasattr(self, 'XLockedDistance') and self.XLockedDistance is not None:
			result = cameraPoint + cameraRay.scale(self.XLockedDistance)
		elif _config_['sniperAS']['distanceLock']['useTargetLock']:
			inputHandler = BigWorld.player().inputHandler
			if hasattr(inputHandler, 'XTargetInfo') and inputHandler.XTargetInfo is not None:
				target = BigWorld.target()
				if target is None or target.id != inputHandler.XTargetInfo.id:
					result = cameraPoint + cameraRay.scale((inputHandler.XTargetInfo.position - cameraPoint).length)
	return result
