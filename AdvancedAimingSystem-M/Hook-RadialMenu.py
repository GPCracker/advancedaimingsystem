#*****
# RadialMenu Hooks
#*****
def new_RadialMenu_currentTarget_getter(self):
	if not hasattr(self, '_currentTarget'):
		self._currentTarget = None
	return self._currentTarget

def new_RadialMenu_currentTarget_setter(self, target):
	if _config_['commonAS']['enableXRayLockForRadialMenu'] and target is None:
		target = XRayScanner.getTarget()
	self._currentTarget = target
	return None
