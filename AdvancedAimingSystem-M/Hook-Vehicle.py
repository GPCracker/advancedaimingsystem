#*****
# Vehicle Hooks
#*****
def new_Vehicle_showDamageFromShot(self, attackerID, points, effectsIndex):
	result = old_Vehicle_showDamageFromShot(self, attackerID, points, effectsIndex)
	if _config_['commonAS']['advancedExpertPerk']['enabled']:
		player = BigWorld.player()
		if hasattr(player, 'XAdvancedExpertPerk') and player.XAdvancedExpertPerk is not None and self.publicInfo['team'] is not player.team:
			if attackerID == player.playerVehicleID:
				player.XAdvancedExpertPerk.queueRequest(self.id, True)
	return result

def new_Vehicle_showDamageFromExplosion(self, attackerID, center, effectsIndex):
	result = old_Vehicle_showDamageFromExplosion(self, attackerID, center, effectsIndex)
	if _config_['commonAS']['advancedExpertPerk']['enabled']:
		player = BigWorld.player()
		if hasattr(player, 'XAdvancedExpertPerk') and player.XAdvancedExpertPerk is not None and self.publicInfo['team'] is not player.team:
			if attackerID == player.playerVehicleID:
				player.XAvancedExpertPerk.queueRequest(self.id, True)
	return result

def new_Vehicle_startVisual(self):
	result = old_Vehicle_startVisual(self)
	if _config_['commonAS']['advancedExpertPerk']['enabled']:
		player = BigWorld.player()
		if hasattr(player, 'XAdvancedExpertPerk') and player.XAdvancedExpertPerk is not None and self.publicInfo['team'] is not player.team:
			pass
	return result

def new_Vehicle_stopVisual(self):
	result = old_Vehicle_stopVisual(self)
	if _config_['commonAS']['advancedExpertPerk']['enabled']:
		player = BigWorld.player()
		if hasattr(player, 'XAdvancedExpertPerk') and player.XAdvancedExpertPerk is not None and self.publicInfo['team'] is not player.team:
			player.XAdvancedExpertPerk.cancelRequest(self.id)
	return result
