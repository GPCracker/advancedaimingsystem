class GUIWrapper(object):
	@classmethod
	def SimpleGUI(sclass, settings):
		gui = GUI.Simple('')
		sclass.applySettings(gui, settings)
		return sclass(gui)
	
	@classmethod
	def TextGUI(sclass, settings):
		gui = GUI.Text('')
		sclass.applySettings(gui, settings)
		return sclass(gui)
	
	@classmethod
	def WindowGUI(sclass, settings):
		gui = GUI.Window('')
		sclass.applySettings(gui, settings)
		return sclass(gui)
	
	@staticmethod
	def applySettings(gui, settings):
		for attribute, value in settings.items():
			getattr(gui, attribute)
			setattr(gui, attribute, value)
		return None
	
	@classmethod
	def SimpleGUIOnly(sclass, settings):
		gui = GUI.Simple('')
		sclass.applySettings(gui, settings)
		return gui
	
	@classmethod
	def TextGUIOnly(sclass, settings):
		gui = GUI.Text('')
		sclass.applySettings(gui, settings)
		return gui
	
	@classmethod
	def WindowGUIOnly(sclass, settings):
		gui = GUI.Window('')
		sclass.applySettings(gui, settings)
		return gui
	
	@staticmethod
	def getColourHexPrefix(vector4):
		return '\c{:02X}{:02X}{:02X}{:02X};'.format(*map(int, vector4.tuple()))
	
	def __init__(self, gui):
		self.__gui = gui
		return None
	
	@property
	def gui(self):
		return self.__gui
	
	def append(self):
		import weakref
		GUI.addRoot(weakref.proxy(self.__gui))
		return None
	
	def remove(self):
		import weakref
		GUI.delRoot(weakref.proxy(self.__gui))
		return None
	
	def __del__(self):
		self.remove()
		return None
