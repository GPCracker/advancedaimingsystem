class SoundEmulator(object):
	duration = pitch = 0.0
	volume = 1.0
	isPlaying = True
	isStolen = muted = paused = False
	name = ''
	state = 'playing'
	getParentCategory = getParentGroup = getParentProject = listParams = param = play = setCallback = stop = stopAndUnload = lambda self, *args, **kwargs: None

class Sound(object):
	def __init__(self, soundName, autoPlay = False, emulateNonExistingEvent = True):
		self.__fmodSound = FMOD.playSound(soundName)
		if self.__fmodSound is None and emulateNonExistingEvent:
			self.__fmodSound = SoundEmulator()
		if not autoPlay:
			self.__fmodSound.stop()
		return None
	
	@property
	def fmodSound(self):
		return self.__fmodSound
	
	def __repr__(self):
		return 'Sound(soundName={})'.format(repr(self.__fmodSound.name))
	
	def __del__(self):
		self.__fmodSound.stop()
		return None

class SoundEvent(object):
	def __init__(self, sound, priority = 0):
		self.sound = sound
		self.priority = priority
		return None
	
	def __call__(self):
		return self.sound.fmodSound.play()
	
	def __repr__(self):
		return 'SoundEvent(sound={}, priority={})'.format(repr(self.sound), repr(self.priority))

class SoundEventQueue(list):
	def __init__(self, maxlen = 10, maxDelay = 2.0):
		self.maxlen = maxlen
		self.maxDelay = maxDelay
		return super(SoundEventQueue, self).__init__()
	
	def update(self, timestamp = None):
		if len(self) > self.maxlen:
			self.remove(min(self, key=lambda soundEventItem: soundEventItem[1]))
		if timestamp is not None:
			for soundEventItem in filter(lambda soundEventItem: soundEventItem[1] + self.maxDelay < timestamp, self):
				self.remove(soundEventItem)
		self.sort(key=lambda soundEventItem: soundEventItem[0].priority)
		return None
	
	def popItem(self, timestamp = None):
		self.update(timestamp)
		return self.pop()[0] if len(self) > 0 else None
	
	def appendItem(self, soundEvent, timestamp):
		self.append((soundEvent, timestamp))
		self.update()
		return None
	
	def __repr__(self):
		return 'SoundEventQueue({}):{}'.format(len(self), super(SoundEventQueue, self).__repr__())

class SoundEventQueueManager(object):
	@staticmethod
	def getCurrentTimestamp():
		return BigWorld.time()
	
	def __init__(self, maxQueueLength = 10, maxDelay = 2.0, eventDelay = 0.5):
		self.__queue = SoundEventQueue(maxQueueLength, maxDelay)
		self.__eventDelay = eventDelay
		self.__queueCallback = None
		return None
	
	def playSoundEvent(self, soundEvent):
		self.__queue.appendItem(soundEvent, self.getCurrentTimestamp())
		if self.__queueCallback is None:
			self.__queueCallback = Callback(0.0, self.__queueCallbackFunc.im_func, self.__queueCallbackFunc.im_self)
		return None
	
	def __handleQueue(self):
		if len(self.__queue) > 0:
			soundEvent = self.__queue.popItem(self.getCurrentTimestamp())
			if soundEvent is not None:
				soundEvent()
				return soundEvent.sound.fmodSound.duration
		return None
	
	def __queueCallbackFunc(self):
		duration = self.__handleQueue()
		if duration is not None:
			self.__queueCallback = Callback(duration + self.__eventDelay, self.__queueCallbackFunc.im_func, self.__queueCallbackFunc.im_self)
		else:
			self.__queueCallback = None
		return None
	
	def __del__(self):
		self.__queueCallback = None
		return None

class SoundEventManager(object):
	def __init__(self):
		self.__events = {}
		self.__sounds = {}
		self.__queues = {}
		return None
	
	def addSoundEvent(self, soundEventName, soundEvent):
		self.__events[soundEventName] = soundEvent
		return None
	
	def removeSoundEvent(self, soundEventName):
		del self.__events[soundEventName]
		return None
	
	def addQueue(self, queueName, maxQueueLength = 10, maxDelay = 2.0, eventDelay = 0.5):
		self.__queues[queueName] = SoundEventQueueManager(maxQueueLength, maxDelay, eventDelay)
		return None
	
	def removeQueue(self, queueName):
		del self.__queues[queueName]
		return None
	
	def playAsyncSoundEvent(self, soundEventName):
		return self.__events[soundEventName]()
	
	def playQueuedSoundEvent(self, soundEventName, queueName):
		return self.__queues[queueName].playSoundEvent(self.__events[soundEventName])
	
	def addAsyncSound(self, soundName, sound):
		self.__sounds[soundName] = sound
		return None
	
	def removeAsyncSound(self, soundName):
		del self.__sounds[soundName]
		return None
	
	def playAsyncSound(self, soundName):
		self.__sounds[soundName].fmodSound.play()
		return None
	
	def stopAsyncSound(self, soundName):
		self.__sounds[soundName].fmodSound.stop()
	
	def getAsyncSoundVolume(self, soundName):
		return self.__sounds[soundName].fmodSound.volume
	
	def setAsyncSoundVolume(self, soundName, volume):
		self.__sounds[soundName].fmodSound.volume = volume
		return None
