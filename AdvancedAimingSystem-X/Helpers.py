def getClientVersion():
	from Account import _readClientServerVersion
	return _readClientServerVersion()[1].split('_')[1]

def isCommonTest():
	from Account import _readClientServerVersion
	return _readClientServerVersion()[1].split('_')[0] == 'ct'

def macrosSubstitution(message, **kwargs):
	import re
	for macros, value in kwargs.items():
		message = re.compile('\{\{' + macros + '\}\}').sub(str(value), message)
	return message

class Callback(object):
	def __init__(self, time, function, *args, **kwargs):
		import functools, weakref
		proxiesL = []
		for arg in args:
			try:
				proxiesL.append(weakref.proxy(arg))
			except TypeError:
				proxiesL.append(arg)
		proxiesD = {}
		for key, value in kwargs.iteritems():
			try:
				proxiesD[key] = weakref.proxy(value)
			except TypeError:
				proxiesD[key] = value
		self.__cbID = BigWorld.callback(time, functools.partial(weakref.proxy(function), *proxiesL, **proxiesD))
		return
	
	def __del__(self):
		try:
			BigWorld.cancelCallback(self.__cbID)
		except ValueError:
			pass
		return

class Event(object):
	def __init__(self):
		self.__delegates = []
		return
	
	def __iadd__(self, delegate):
		if delegate not in self.__delegates:
			self.__delegates.append(delegate)
		return self
	
	def __isub__(self, delegate):
		if delegate in self.__delegates:
			self.__delegates.remove(delegate)
		return self
	
	def __call__(self, *args, **kwargs):
		for delegate in self.__delegates:
			try:
				delegate(*args, **kwargs)
			except:
				import traceback
				traceback.print_exc()
		return
	
	def __repr__(self):
		return 'Event({}):{}'.format(len(self.__delegates), repr(self.__delegates))

class Trigger(object):
	def __init__(self, status = False, onActivate = None, onDeactivate = None, onChange = None):
		self.__status = status
		self.onActivate = onActivate if onActivate is not None else Event()
		self.onDeactivate = onDeactivate if onDeactivate is not None else Event()
		self.onChange = onChange if onChange is not None else Event()
		return
	
	@property
	def status(self):
		return self.__status
	
	@status.setter
	def status(self, value):
		if value and not self.__status:
			self.onActivate()
			self.onChange(True)
		elif not value and self.__status:
			self.onDeactivate()
			self.onChange(False)
		self.__status = bool(value)
		return
	
	def enable(self):
		self.status = True
		return
	
	def disable(self):
		self.status = False
		return
	
	def __repr__(self):
		return 'Trigger(status = {}, onActivate = {}, onDeactivate = {}, onChange = {})'.format(
			repr(self.status),
			repr(self.onActivate),
			repr(self.onDeactivate),
			repr(self.onChange)
		)

class DelayTrigger(Trigger):
	def __init__(self, timestampGetter, dynamicStatus = False, status = False, activateDelay = 0.0, deactivateDelay = 0.0, onActivate = None, onDeactivate = None, onChange = None):
		self.__timestampGetter = timestampGetter
		self.__dynamicStatus = dynamicStatus
		self.__dynamicStatusTimestamp = None
		self.__activateDelay = activateDelay
		self.__deactivateDelay = deactivateDelay
		return super(DelayTrigger, self).__init__(status, onActivate, onDeactivate, onChange)
	
	def __repr__(self):
		return 'DelayTrigger(dynamicStatus = {}, status = {}, activateDelay = {}, deactivateDelay = {}, onActivate = {}, onDeactivate = {}, onChange = {})'.format(
			repr(self.dynamicStatus),
			repr(self.status),
			repr(self.__activateDelay),
			repr(self.__deactivateDelay),
			repr(self.onActivate),
			repr(self.onDeactivate),
			repr(self.onChange)
		)
	
	@property
	def dynamicStatus(self):
		return self.__dynamicStatus
	
	@dynamicStatus.setter
	def dynamicStatus(self, value):
		delay = self.__activateDelay if self.__dynamicStatus else self.__deactivateDelay
		if value != self.__dynamicStatus:
			self.__dynamicStatus = value
			self.__dynamicStatusTimestamp = self.__timestampGetter()
		elif self.__dynamicStatusTimestamp + delay < self.__timestampGetter():
			self.status = self.__dynamicStatus
		return
	
	def dynamicEnable(self):
		self.dynamicStatus = True
		return
	
	def dynamicDisable(self):
		self.dynamicStatus = False
		return

class ThreeStateTrigger(object):
	def __init__(self, status = None, onActivate = None, onDeactivate = None, onReset = None, onChange = None):
		self.__status = status
		self.onActivate = onActivate if onActivate is not None else Event()
		self.onDeactivate = onDeactivate if onDeactivate is not None else Event()
		self.onReset = onReset if onReset is not None else Event()
		self.onChange = onChange if onChange is not None else Event()
		return
	
	@property
	def status(self):
		return self.__status
	
	@status.setter
	def status(self, value):
		if value != self.__status:
			if value == True:
				self.onActivate()
			elif value == False:
				self.onDeactivate()
			elif value == None:
				self.onReset()
			else:
				raise TypeError()
			self.onChange(value)
			self.__status = value
		return
	
	def enable(self):
		self.status = True
		return
	
	def disable(self):
		self.status = False
		return
	
	def reset(self):
		self.status = None
		return
	
	def __repr__(self):
		return 'ThreeStateTrigger(status = {}, onActivate = {}, onDeactivate = {}, onReset = {}, onChange = {})'.format(
			repr(self.status),
			repr(self.onActivate),
			repr(self.onDeactivate),
			repr(self.onReset),
			repr(self.onChange)
		)

class ConditionTrigger(ThreeStateTrigger):
	def __init__(self, variable = None, comparator = lambda variable: None, onActivate = None, onDeactivate = None, onReset = None, onChange = None):
		self.__variable = variable
		self.__comparator = comparator
		return super(ConditionTrigger, self).__init__(comparator(variable), onActivate, onDeactivate, onReset, onChange)
	
	@property
	def comparator(self):
		return self.__comparator
	
	@comparator.setter
	def comparator(self, value):
		self.__comparator = value
		self.status = self.__comparator(self.__variable)
		return
	
	@property
	def variable(self):
		return self.__variable
	
	@variable.setter
	def variable(self, value):
		self.__variable = value
		self.status = self.__comparator(self.__variable)
		return
	
	def __repr__(self):
		return 'ConditionTrigger(variable = {}, comparator = {}, onActivate = {}, onDeactivate = {}, onReset = {}, onChange = {})'.format(
			repr(self.__variable),
			repr(self.__comparator),
			repr(self.onActivate),
			repr(self.onDeactivate),
			repr(self.onReset),
			repr(self.onChange)
		)
