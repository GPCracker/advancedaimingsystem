class ParseError(Exception):
	pass

def keyEventParse(event):
	'''
	keyEventParse(event) -> (key, isDown, isRepeat, mods)
	mods sum: Shift = 1; Ctrl = 2; Alt = 4;
	'''
	modKeys = {Keys.KEY_LSHIFT: {'pair': Keys.KEY_RSHIFT, 'shift': 0}, Keys.KEY_RSHIFT: {'pair': Keys.KEY_LSHIFT, 'shift': 0}, Keys.KEY_LCONTROL: {'pair': Keys.KEY_RCONTROL, 'shift': 1}, Keys.KEY_RCONTROL: {'pair': Keys.KEY_LCONTROL, 'shift': 1}, Keys.KEY_LALT: {'pair': Keys.KEY_RALT, 'shift': 2}, Keys.KEY_RALT: {'pair': Keys.KEY_LALT, 'shift': 2}}
	mods = event.modifiers & ~((1 << modKeys[event.key]['shift']) if event.key in modKeys.keys() and not BigWorld.isKeyDown(modKeys[event.key]['pair']) else 0)
	return (event.key, event.isKeyDown(), event.isRepeatedEvent(), mods)

def parseHotKeySequence(hkSequence):
	'''
	parseHotKeySequence(hkSequence) -> (key, mods)
	mods sum: Shift = 1; Ctrl = 2; Alt = 4;
	'''
	modKeys = {Keys.KEY_LSHIFT: {'pair': Keys.KEY_RSHIFT, 'shift': 0}, Keys.KEY_RSHIFT: {'pair': Keys.KEY_LSHIFT, 'shift': 0}, Keys.KEY_LCONTROL: {'pair': Keys.KEY_RCONTROL, 'shift': 1}, Keys.KEY_RCONTROL: {'pair': Keys.KEY_LCONTROL, 'shift': 1}, Keys.KEY_LALT: {'pair': Keys.KEY_RALT, 'shift': 2}, Keys.KEY_RALT: {'pair': Keys.KEY_LALT, 'shift': 2}}
	hkSequence = map(lambda key: getattr(Keys, key, None), hkSequence.split('+'))
	if filter(lambda key: key is None, hkSequence):
		raise ParseError('One or more keys can not be recognized.')
	key = hkSequence.pop()
	key = key if key is not Keys.KEY_NONE else -1
	if filter(lambda mod: mod not in modKeys.keys(), hkSequence):
		raise ParseError('Only Shift, Ctrl and Alt could be modifiers.')
	mods = sum([1 << modKeys[mod]['shift'] for mod in hkSequence])
	return key, mods
