class Umlaut(object):
	@staticmethod
	def readURItem(section):
		uml = section.readWideString('uml', u'')
		rpl = section.readWideString('rpl', u'')
		return (uml, rpl) if uml != u'' and rpl != u'' else None
	
	@staticmethod
	def readURSection(section):
		return filter(lambda pair: pair is not None, [Umlaut.readURItem(item) for name, item in section.items() if name == 'umlaut'])
	
	def __init__(self, section = None, xml = None):
		if section is None:
			section = ResMgr.openSection(xml)
		assert section
		self.pairs = self.readURSection(section)
		return None
	
	def replace(self, str):
		for uml, rpl in self.pairs:
			if uml in str:
				str = str.replace(uml, rpl)
		return str
