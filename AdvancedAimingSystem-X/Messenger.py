def getBattleChatControllers():
	from messenger import MessengerEntry
	from constants import PREBATTLE_TYPE
	from messenger.m_constants import PROTO_TYPE
	result = {'squad': None, 'team': None, 'common': None}
	channelsCtrl = MessengerEntry.g_instance.gui.channelsCtrl
	for controller in channelsCtrl.getControllersIterator():
		channel = controller.getChannel()
		if channel.getProtoType() is PROTO_TYPE.BW_CHAT2 and channel.getClientID() == -130:
			result['common'] = controller
		elif channel.getProtoType() is PROTO_TYPE.BW_CHAT2 and channel.getClientID() == -129:
			result['team'] = controller
		elif channel.getProtoType() is PROTO_TYPE.BW_CHAT2 and channel.getClientID() in [-PREBATTLE_TYPE.SQUAD]:
			result['squad'] = controller
	return result

def showMessageOnPanel(panel, key, msgText, color):
	from gui.WindowsManager import g_windowsManager
	if g_windowsManager.battleWindow is not None and panel in ['VehicleErrorsPanel', 'VehicleMessagesPanel', 'PlayerMessagesPanel']:
		g_windowsManager.battleWindow.call('battle.' + panel + '.ShowMessage', [key, msgText, color])
	return

def showMessageInChat(msgText):
	from messenger import MessengerEntry
	if MessengerEntry.g_instance is not None:
		MessengerEntry.g_instance.gui.addClientMessage(msgText)
	return
