def isVehicleAlly(vehicleID):
	player = BigWorld.player()
	return player.team is player.arena.vehicles[vehicleID]['team']

def isVehicleEnemy(vehicleID):
	player = BigWorld.player()
	return player.team is not player.arena.vehicles[vehicleID]['team']

def isVehicleSquad(vehicleID):
	import gui.battle_control
	return gui.battle_control.g_sessionProvider.getCtx().isSquadMan(vehicleID)

def isVehiclePlayer(vehicleID):
	return vehicleID == BigWorld.player().playerVehicleID

def isVehicleVisible(vehicleID):
	return BigWorld.entity(vehicleID) is not None

def isVehicleTarget(vehicleID):
	target = BigWorld.target()
	return target is not None and target.id == vehicleID

def isVehicleAutoAim(vehicleID):
	autoAimVehicle = BigWorld.player().autoAimVehicle
	return autoAimVehicle is not None and autoAimVehicle.id == vehicleID

def isVehicleTeamKiller(vehicleID):
	import gui.battle_control
	return gui.battle_control.g_sessionProvider.getCtx().isTeamKiller(vehicleID)

def isVehicleAlive(vehicleID, useArena = True):
	if not useArena:
		vehicle = BigWorld.entity(vehicleID)
		return vehicle.isAlive() if vehicle is not None else None
	return BigWorld.player().arena.vehicles[vehicleID]['isAlive']

def getVehicleClass(vehicleID):
	from items.vehicles import VEHICLE_CLASS_TAGS
	classTags = BigWorld.player().arena.vehicles[vehicleID]['vehicleType'].type.tags & VEHICLE_CLASS_TAGS
	return set(classTags).pop() if len(classTags) > 0 else None

def getBattleScore():
	battleScore = {'totalAllies': 0, 'aliveAllies': 0, 'fragsAllies': 0, 'totalEnemies': 0, 'aliveEnemies': 0, 'fragsEnemies': 0}
	arena = BigWorld.player().arena
	for vID, vData in arena.vehicles.items():
		vStats = arena.statistics.get(vID, None)
		if vData['team'] is BigWorld.player().team:
			battleScore['totalAllies'] += 1
			battleScore['aliveAllies'] += 1 if vData['isAlive'] else 0
			battleScore['fragsAllies'] += 0 if vStats is None else vStats['frags']
		else:
			battleScore['totalEnemies'] += 1
			battleScore['aliveEnemies'] += 1 if vData['isAlive'] else 0
			battleScore['fragsEnemies'] += 0 if vStats is None else vStats['frags']
	return battleScore

def getVehicleTags(vehicleID, useArena = True):
	vehicleTags = set()
	vehicleTags.add('alive' if isVehicleAlive(vehicleID, useArena) else '')
	vehicleTags.add('enemy' if isVehicleEnemy(vehicleID) else 'player' if isVehiclePlayer(vehicleID) else 'ally' if isVehicleAlly(vehicleID) else '')
	vehicleTags.add('visible' if isVehicleVisible(vehicleID) else '')
	vehicleTags.add('teamKiller' if isVehicleTeamKiller(vehicleID) else '')
	vehicleTags.add('squad' if isVehicleSquad(vehicleID) else '')
	vehicleTags.add('target' if isVehicleTarget(vehicleID) else '')
	vehicleTags.add('autoAim' if isVehicleAutoAim(vehicleID) else '')
	vehicleTags.remove('')
	vehicleTags.add(getVehicleClass(vehicleID))
	return frozenset(vehicleTags)

def filterVehicleTags(vehicleTags):
	from items.vehicles import VEHICLE_CLASS_TAGS
	vehicleTagsList = ['alive', 'enemy', 'player', 'ally', 'visible', 'teamKiller', 'squad', 'target', 'autoAim']
	vehicleTagsList.extend(VEHICLE_CLASS_TAGS)
	return filter(lambda vehicleTag: vehicleTag in vehicleTagsList, vehicleTags)
