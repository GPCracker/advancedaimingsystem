class ActiveRequest(object):
	def __init__(self, vehicleID):
		self.vehicleID = vehicleID
		self.time = BigWorld.time()
		BigWorld.player().cell.monitorVehicleDamagedDevices(vehicleID)
		return None
	
	def __del__(self):
		BigWorld.player().cell.monitorVehicleDamagedDevices(0)
		return None

class AdvancedExpertPerk(object):
	def __init__(self, expertStatusFunc, cacheExtrasInfo = False, enableQueue = False, replyInterval = 5.0, requestInterval = 5.0):
		self.__expertStatusFunc = expertStatusFunc
		self.__cacheExtrasInfo = cacheExtrasInfo
		self.__enableQueue = enableQueue
		self.__replyInterval = replyInterval
		self.__requestInterval = requestInterval
		self.__extrasInfoCache = {}
		self.__activeRequest = None
		self.__requestQueue = []
		self.__checkRequestCB = None
		self.__checkIdleCB = None
		return None
	
	@property
	def maySeeOtherVehicleDamagedDevices(self):
		return self.__expertStatusFunc() if self.__expertStatusFunc is not None else False
	
	def getCachedExtrasInfo(self, vehicleID):
		return self.__extrasInfoCache.get(vehicleID, None)
	
	@staticmethod
	def isAlive(vehicleID):
		arenaVehicles = BigWorld.player().arena.vehicles
		return vehicleID in arenaVehicles.keys() and arenaVehicles[vehicleID]['isAlive']
	
	@staticmethod
	def isVisible(vehicleID):
		return BigWorld.entity(vehicleID) is not None
	
	@property
	def activeRequest(self):
		return self.__activeRequest
	
	@activeRequest.setter
	def activeRequest(self, value):
		self.__activeRequest = value
		if value is not None:
			self.__checkIdleCB = None
			self.__checkRequestCB = Callback(self.__replyInterval * 1.1, self.__checkRequest.__func__, self, value.vehicleID)
		else:
			self.__checkRequestCB = None
			self.__checkIdleCB = Callback(self.__requestInterval * 1.1, self.__checkIdle.__func__, self)
		return None
	
	def requestExtrasInfo(self, vehicleID):
		if self.maySeeOtherVehicleDamagedDevices and self.isVisible(vehicleID):
			if self.activeRequest is None or self.activeRequest.vehicleID != vehicleID:
				self.activeRequest = ActiveRequest(vehicleID)
		return None
	
	def cancelRequest(self, vehicleID = None):
		if self.activeRequest is not None and (vehicleID is None or self.activeRequest.vehicleID == vehicleID):
			self.activeRequest = None
		return None
	
	def cancelQueueRequest(self, vehicleID):
		if vehicleID in self.__requestQueue:
			self.__requestQueue.remove(vehicleID)
		return None
	
	def queueRequest(self, vehicleID, maxPriority = False):
		self.cancelQueueRequest(vehicleID)
		if self.__cacheExtrasInfo and self.__enableQueue and self.isAlive(vehicleID):
			if maxPriority:
				self.__requestQueue.insert(0, vehicleID)
			else:
				self.__requestQueue.append(vehicleID)
		return None
	
	def onExtrasInfoReceived(self, vehicleID, damagedExtras, destroyedExtras):
		if self.__cacheExtrasInfo and self.isAlive(vehicleID):
			self.__extrasInfoCache[vehicleID] = (damagedExtras, destroyedExtras)
		self.cancelRequest(vehicleID)
		self.cancelQueueRequest(vehicleID)
		return None
	
	def __checkRequest(self, vehicleID):
		if self.activeRequest is not None and self.activeRequest.vehicleID == vehicleID and self.activeRequest.time + self.__replyInterval <= BigWorld.time():
			self.cancelRequest(vehicleID)
		return None
	
	def __checkIdle(self):
		self.__requestQueue = filter(self.isAlive, self.__requestQueue)
		if self.activeRequest is None:
			visible = filter(self.isVisible, self.__requestQueue)
			if visible:
				self.requestExtrasInfo(visible[0])
		return None
	
	def __del__(self):
		self.__checkRequestCB = None
		self.__checkIdleCB = None
		return None
