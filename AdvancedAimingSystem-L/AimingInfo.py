class AimingInfo(object):
	@staticmethod
	def getPlayerShotRayAndPoint():
		vehicleTypeDescriptor = BigWorld.player().vehicleTypeDescriptor
		vehicleMatrix = BigWorld.player().getOwnVehicleMatrix()
		turretYaw = BigWorld.player().gunRotator.turretYaw
		gunPitch = BigWorld.player().gunRotator.gunPitch
		return getShotRayAndPoint(vehicleTypeDescriptor, vehicleMatrix, turretYaw, gunPitch)
	
	@staticmethod
	def getAimingInfo():
		'''
		dispersionAngle - gun dispersion angle (static gun property),
		aimingStartTime - time when aiming started,
		aimingStartFactor - dispersion factor when aiming started,
		dispersionFactor - gun dispersion angle factor (gun condition),
		aimingTime - aiming exp time.
		'''
		aimingInfo = BigWorld.player()._PlayerAvatar__aimingInfo
		vehicleTypeDescriptor = BigWorld.player().vehicleTypeDescriptor
		gunMarkerPosition = BigWorld.player().gunRotator.markerInfo[0]
		playerShotPoint = AimingInfo.getPlayerShotRayAndPoint()[1]
		return {
			'dispersionAngle': vehicleTypeDescriptor.gun['shotDispersionAngle'],
			'aimingDistance': playerShotPoint.distTo(gunMarkerPosition),
			'shotSpeed': vehicleTypeDescriptor.shot['speed'],
			'aimingStartTime': aimingInfo[0],
			'aimingStartFactor': aimingInfo[1],
			'dispersionFactor': aimingInfo[2],
			'aimingTime': aimingInfo[6]
		}
	
	@staticmethod
	def getAimingFactor(aimingInfo):
		#Every <aimingTime> seconds dispersion decreases EXP times.
		import math
		deltaTime = aimingInfo['aimingStartTime'] - BigWorld.time()
		deltaFactor = aimingInfo['aimingStartFactor'] / aimingInfo['dispersionFactor']
		if abs(deltaFactor) < 1.05:
			return aimingInfo['dispersionFactor']
		return aimingInfo['aimingStartFactor'] * math.exp(deltaTime / aimingInfo['aimingTime'])
	
	@staticmethod
	def getFullAimingTime(aimingInfo):
		#Calculates time required for dispersion decreasing <aimingStartFactor>/<shotDispersionFactor> times.
		import math
		return aimingInfo['aimingTime'] * math.log(aimingInfo['aimingStartFactor'] / aimingInfo['dispersionFactor'])
	
	@staticmethod
	def getRemainingAimingTime(aimingInfo, fullAimingTime):
		return max(0.0, aimingInfo['aimingStartTime'] + fullAimingTime - BigWorld.time())
	
	@staticmethod
	def getDispersionAngle(aimingInfo, aimingFactor):
		return aimingInfo['dispersionAngle'] * aimingFactor
	
	@staticmethod
	def getDeviation(aimingInfo, dispersionAngle):
		return aimingInfo['aimingDistance'] * dispersionAngle
	
	@staticmethod
	def getFlyTime(aimingInfo):
		return aimingInfo['aimingDistance'] / aimingInfo['shotSpeed']
	
	def __init__(self, windowSettings, textSettings):
		self.window = GUIWrapper.WindowGUIOnly(windowSettings)
		self.window.label = GUIWrapper.TextGUIOnly(textSettings)
		self.window.visible = False
		import weakref
		GUI.addRoot(weakref.proxy(self.window))
		return None
	
	def formatTemplate(self, template):
		try:
			aimingInfo = self.getAimingInfo()
			staticDispersionAngle = aimingInfo['dispersionAngle'] * aimingInfo['dispersionFactor']
			aimingDistance = aimingInfo['aimingDistance']
			shotSpeed = aimingInfo['shotSpeed']
			aimingTime = aimingInfo['aimingTime']
			aimingFactor = self.getAimingFactor(aimingInfo)
			fullAimingTime = self.getFullAimingTime(aimingInfo)
			remainingAimingTime = self.getRemainingAimingTime(aimingInfo, fullAimingTime)
			dispersionAngle = self.getDispersionAngle(aimingInfo, aimingFactor)
			deviation = self.getDeviation(aimingInfo, dispersionAngle)
			flyTime = self.getFlyTime(aimingInfo)
			message = formatMessage(template.replace('\\n', '\n'), staticDispersionAngle = round(staticDispersionAngle, 5), aimingDistance = round(aimingDistance, 0), shotSpeed = round(shotSpeed, 0), aimingTime = round(aimingTime, 1), remainingAimingTime = round(remainingAimingTime, 1), dispersionAngle = round(dispersionAngle, 5), deviation = round(deviation, 2), flyTime = round(flyTime, 2))
		except:
			message = ''
		return message
	
	def __del__(self):
		import weakref
		GUI.delRoot(weakref.proxy(self.window))
		return None
