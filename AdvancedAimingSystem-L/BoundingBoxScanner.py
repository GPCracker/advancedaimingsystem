class BoundingBoxScanner(object):
	@staticmethod
	def getActiveVehicles(fltr = None):
		activeVehicles = []
		for vehicleID in filter(fltr, BigWorld.player().arena.vehicles.iterkeys()):
			if vehicleID == BigWorld.player().playerVehicleID:
				continue
			vehicle = BigWorld.entity(vehicleID)
			if vehicle is not None and vehicle.isStarted:
				activeVehicles.append(vehicle)
		return activeVehicles
	
	@staticmethod
	def getVehicleBoundingBox(vehicle, scalar = 1.0):
		vehicleBB = BoundingBox.getModelBoundingBox(vehicle.model)
		vehicleBB.scale(scalar)
		return vehicleBB
	
	@staticmethod
	def scanTarget(scanStart, scanDir, maxDistance, entities, scalar = 1.0):
		scanStop = scanStart + scanDir * maxDistance
		collisionResults = filter(lambda collisionResult: collisionResult[1], [(entity, BoundingBoxScanner.getVehicleBoundingBox(entity, scalar).intersectSegment(scanStart, scanStop)) for entity in entities])
		return collisionResults[0][0] if len(collisionResults) == 1 else None
	
	@staticmethod
	def getTarget(maxDistance = 720, scalar = 1.0, fltr = None):
		from AvatarInputHandler.cameras import getWorldRayAndPoint
		scanDir, scanStart = getWorldRayAndPoint(*BigWorld.player().inputHandler.ctrl.getAim().offset())
		scanDir.normalise()
		return BoundingBoxScanner.scanTarget(scanStart, scanDir, maxDistance, BoundingBoxScanner.getActiveVehicles(fltr), scalar)
