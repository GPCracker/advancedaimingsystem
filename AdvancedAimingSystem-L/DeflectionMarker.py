class DeflectionMarker(object):
	@staticmethod
	def calculateHitPoint(targetPoint, targetVector, shotPoint, shotVectorLength):
		import math
		speedRatio = targetVector.length / shotVectorLength
		distanceVector = targetPoint - shotPoint
		cosB = min(1.0, max(-1.0, targetVector.dot(-distanceVector) / (targetVector.length + distanceVector.length)))
		A = 1.0 - speedRatio ** 2.0
		B = 2.0 * speedRatio * distanceVector.length * cosB
		C = -distanceVector.lengthSquared
		D = B ** 2.0 - 4.0 * A * C
		assert D >= 0.0
		shotPathLength = (-B + math.sqrt(D)) / (2.0 * A)
		return targetPoint + targetVector * shotPathLength / shotVectorLength
	
	@staticmethod
	def getScreenPoint(worldPoint):
		from AvatarInputHandler.cameras import projectPoint
		screenPoint = projectPoint(worldPoint)
		return (screenPoint.x, screenPoint.y) if screenPoint.w != 0 else None
	
	def __init__(self, settings):
		self.__display = False
		self.marker = GUI.Simple('')
		for attribute, value in settings.items():
			getattr(self.marker, attribute)
			setattr(self.marker, attribute, value)
		self.marker.visible = False
		import weakref
		GUI.addRoot(weakref.proxy(self.marker))
		return None
	
	@property
	def display(self):
		return self.__display
	
	@display.setter
	def display(self, value):
		self.__display = value
		self.marker.visible = False
		return None
	
	def updateMarker(self, targetInfo):
		if self.__display and targetInfo is not None and targetInfo.isVisible:
			shotPoint, shotVector, shotGravity, shotMaxDistance = getPlayerVehicleShotParams()
			hitPoint = self.calculateHitPoint(targetInfo.position, targetInfo.velocity, shotPoint, shotVector.length) + targetInfo.heightVector.scale(0.5)
			screenPoint = self.getScreenPoint(hitPoint)
			if screenPoint is not None:
				self.marker.position = screenPoint + (1.0, )
				self.marker.visible = True
				return None
		self.marker.visible = False
		return None
	
	def __del_(self):
		import weakref
		GUI.delRoot(weakref.proxy(self.marker))
		return None
