def rotationMatrix(yaw, pitch, roll):
	result = Math.Matrix()
	result.setRotateYPR((yaw, pitch, roll))
	return result

def computeProjectileTrajectoryEnd(vehicleTypeDescriptor, vehicleMatrix, turretYaw, gunPitch, colliders):
	import constants
	shotRay, shotPoint = getShotRayAndPoint(vehicleTypeDescriptor, vehicleMatrix, turretYaw, gunPitch)
	shotSpeed = shotRay.scale(vehicleTypeDescriptor.shot['speed'])
	shotGravity = Math.Vector3(0, -1, 0).scale(vehicleTypeDescriptor.shot['gravity'])
	currentPoint, currentSpeed = Math.Vector3(shotPoint), Math.Vector3(shotSpeed)
	hitPoint, hitVector = None, None
	while True:
		checkPoints = BigWorld.wg_computeProjectileTrajectory(currentPoint, currentSpeed, shotGravity, constants.SERVER_TICK_LENGTH, constants.SHELL_TRAJECTORY_EPSILON_CLIENT)
		collisionTestStart = currentPoint
		for collisionTestStop in checkPoints:
			for collider in colliders:
				collisionResult = collider(collisionTestStart, collisionTestStop)
				if isinstance(collisionResult, (list, tuple)):
					collisionResult = collisionResult[0] if collisionResult else None
				if collisionResult is not None:
					hitPoint = collisionResult
					hitVector = collisionTestStop - collisionTestStart
					hitVector.normalise()
					break
			if hitPoint is not None and hitVector is not None:
				break
			collisionTestStart = collisionTestStop
		if hitPoint is not None and hitVector is not None:
			break
		currentPoint += currentSpeed.scale(constants.SERVER_TICK_LENGTH) + shotGravity.scale(constants.SERVER_TICK_LENGTH ** 2 * 0.5)
		currentSpeed += shotGravity.scale(constants.SERVER_TICK_LENGTH)
	return hitPoint, hitVector

def computeAPbyMP(mapPoint):
	scanStart = Math.Vector3(mapPoint.x, 1000.0, mapPoint.z)
	scanStop = Math.Vector3(mapPoint.x, -250.0, mapPoint.z)
	scanResult = BigWorld.wg_collideSegment(BigWorld.player().spaceID, scanStart, scanStop, 128)
	return scanResult[0] if scanResult is not None else None

def computeAPbyPP(planePosition, externalColliders = []):
	vehicleTypeDescriptor = BigWorld.player().vehicleTypeDescriptor
	vehicleMatrix = BigWorld.player().getOwnVehicleMatrix()
	turretYaw, gunPitch = getShotAngles(vehicleTypeDescriptor, vehicleMatrix, planePosition)
	def collideStatic(collisionTestStart, collisionTestStop):
		collisionResult = BigWorld.wg_collideSegment(BigWorld.player().spaceID, collisionTestStart, collisionTestStop, 128)#3
		return collisionResult[0] if collisionResult is not None else None
	spaceBB = BoundingBox.getSpaceBoundingBox()
	return computeProjectileTrajectoryEnd(vehicleTypeDescriptor, vehicleMatrix, turretYaw, gunPitch, externalColliders + [collideStatic, spaceBB.intersectSegment])

def computePPbyAP(aimingPoint, zeroPlaneLevel):
	vehicleTypeDescriptor = BigWorld.player().vehicleTypeDescriptor
	vehicleMatrix = BigWorld.player().getOwnVehicleMatrix()
	turretYaw, gunPitch = getShotAngles(vehicleTypeDescriptor, vehicleMatrix, aimingPoint)
	xzPlane = Plane(Math.Vector3(0, zeroPlaneLevel, 0), Math.Vector3(0, 1, 0))
	return computeProjectileTrajectoryEnd(vehicleTypeDescriptor, vehicleMatrix, turretYaw, gunPitch, [xzPlane.intersectSegment])

class RangeCollider(object):
	def __init__(self):
		self.range = None
		return None
	
	@staticmethod
	def getOwnVehiclePosition():
		return BigWorld.player().getOwnVehiclePosition()
	
	@staticmethod
	def getDesiredShotPoint():
		return BigWorld.player().inputHandler.ctrl.camera.aimingSystem.getDesiredShotPoint()
	
	def __call__(self, collisionTestStart, collisionTestStop):
		if self.range is not None:
			assert self.range > 0
			ownVehiclePosition = self.getOwnVehiclePosition()
			if ownVehiclePosition.flatDistTo(collisionTestStart) < self.range and ownVehiclePosition.flatDistTo(collisionTestStop) > self.range:
				return collisionTestStart + (collisionTestStop - collisionTestStart).scale((self.range - ownVehiclePosition.flatDistTo(collisionTestStart)) / collisionTestStart.flatDistTo(collisionTestStop))
		return None
	
	def resetRange(self):
		desiredShotPoint = self.getDesiredShotPoint()
		if desiredShotPoint is not None:
			self.range = self.getOwnVehiclePosition().flatDistTo(desiredShotPoint)
		return None
