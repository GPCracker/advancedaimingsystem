def getCameraRayAndPoint():
	camera = BigWorld.camera()
	return camera.direction, camera.position

def formatMessage(message, **kwargs):
	import re
	for macros, value in kwargs.items():
		pattern = re.compile('\{\{' + macros + '\}\}')
		message = pattern.sub(str(value), message)
	pattern = re.compile('\{\{\w*\}\}')
	message = pattern.sub('undefined', message)
	return message

def getGunMarkerData():
	#markerPos, markerDir, markerSize, collData
	gunRotator = BigWorld.player().gunRotator
	shotPos, shotVec = gunRotator._VehicleGunRotator__getCurShotPosition()
	return gunRotator._VehicleGunRotator__getGunMarkerPosition(shotPos, shotVec, gunRotator._VehicleGunRotator__dispersionAngle)
