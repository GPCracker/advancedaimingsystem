class XRayScanner(object):
	@staticmethod
	def getActiveVehicles(fltr = None):
		activeVehicles = []
		for vehicleID in filter(fltr, BigWorld.player().arena.vehicles.iterkeys()):
			if vehicleID == BigWorld.player().playerVehicleID:
				continue
			vehicle = BigWorld.entity(vehicleID)
			if vehicle is not None and vehicle.isStarted:
				activeVehicles.append(vehicle)
		return activeVehicles
	
	@staticmethod
	def scanXRay(scanStart, scanDir, maxDistance, entities, skipGun = False):
		collisionResults = filter(lambda collisionResult: collisionResult[1] is not None, [(entity, entity.collideSegment(scanStart, scanStart + scanDir * maxDistance, skipGun)) for entity in entities])
		collisionResults.sort(key = lambda collisionResult: collisionResult[1].dist)
		return {
			'scanStart': scanStart,
			'scanDir': scanDir,
			'distance': collisionResults[0][1].dist,
			'hitPoint': scanStart + scanDir * collisionResults[0][1].dist,
			'entity': collisionResults[0][0],
			'hitAngleCos': collisionResults[0][1].hitAngleCos,
			'armor': collisionResults[0][1].armor
		} if len(collisionResults) > 0 else None
	
	@staticmethod
	def getTarget(maxDistance = 720, fltr = None, skipGun = False):
		from AvatarInputHandler.cameras import getWorldRayAndPoint
		scanDir, scanStart = getWorldRayAndPoint(*BigWorld.player().inputHandler.ctrl.getAim().offset())
		scanDir.normalise()
		scanResult = XRayScanner.scanXRay(scanStart, scanDir, maxDistance, XRayScanner.getActiveVehicles(fltr), skipGun)
		return scanResult['entity'] if scanResult is not None else None
