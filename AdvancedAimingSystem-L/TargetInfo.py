class TargetInfo(object):
	@staticmethod
	def isVehicle(entity):
		import Vehicle
		return isinstance(entity, Vehicle.Vehicle)
	
	@staticmethod
	def getVehicleShortName(vehicleID):
		return BigWorld.player().arena.vehicles.get(vehicleID)['vehicleType'].type.shortUserString
	
	def __init__(self, target, lockTime = None):
		assert self.isVehicle(target)
		self.id = target.id
		self.shortName = self.getVehicleShortName(target.id)
		self.physics = target.typeDescriptor.physics
		self.__position = target.position
		self.__heightVector = getVehicleHeightVector(target)
		self.lockTime = lockTime
		return None
	
	@property
	def isAutoLocked(self):
		return self.lockTime is not None
	
	@property
	def entity(self):
		return BigWorld.entity(self.id)
	
	@property
	def isVisible(self):
		return BigWorld.entity(self.id) is not None
	
	@property
	def speed(self):
		vehicle = BigWorld.entity(self.id)
		if vehicle is not None:
			return abs(vehicle.filter.speedInfo.value[0])
		return None
	
	@property
	def velocity(self):
		vehicle = BigWorld.entity(self.id)
		if vehicle is not None:
			return vehicle.velocity
		return None
	
	@property
	def position(self):
		vehicle = BigWorld.entity(self.id)
		if vehicle is not None:
			self.__position = vehicle.position
		return self.__position
	
	@property
	def savedPosition(self):
		return self.__position
	
	@property
	def heightVector(self):
		vehicle = BigWorld.entity(self.id)
		if vehicle is not None:
			self.__heightVector = getVehicleHeightVector(vehicle)
		return self.__heightVector
	
	@property
	def savedHeightVector(self):
		return self.__heightVector
