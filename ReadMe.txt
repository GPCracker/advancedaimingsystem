#Disclaimer:
#	This file was written in Russian, and partially translated by special software.
#	Configuration file was translated by author himself in case of preventing data distortion.
#	Author is not responsible for the accuracy of any other translations except Russian below.

Улучшенная система прицеливания / Advanced Aiming System
Модификация для World of Tanks / Modification for World of Tanks

Автор / Author: GPCracker

[Описание модификации]
Изначально модификация разрабатывалась как автоматизированная замена моду "Баллистический вычислитель". Однако позднее в модификацию было добавлено большое количество новых функций.
На текущий момент последняя версия модификации (0.0.8 Alpha #2) имеет следующий функционал:
	- Модификация может корректировать положение точки прицеливания, тем самым ликвидируя перелеты при стрельбе с большим упреждением по быстро движущейся цели (корректировка дистанции). Подробнее в "Алгоритм работы: аркадный режим".
	- Модификация может блокировать точку прицеливания по высоте в артиллерийском режиме, тем самым позволяет закидывать под различные укрытия, когда танк противника не виден сверху (мосты, вокзалы).
	- Модификация может приподнимать точку прицеливания над землей в артиллерийском режиме для упрощения стрельбы с упреждением на точных артиллериях на сложной местности. Подробнее в "Алгоритм работы: артиллерийский режим".
	- Корректировка точки прицеливания в аркадном режиме может осуществляться как в ручном, так и автоматическом режиме на основе данных "захвата цели". Подробнее в "Алгоритм работы: захват цели".
	- В артиллерийском режиме абсолютная высота корректируется вручную, а относительная поправка по высоте - автоматически.
	- При захвате цели, а также при корректировке точки прицеливания может выводиться на экран дополнительная информация.
	- Захват целей в автоприцел рентгеном (сквозь текстуры при точном попадании прицела в цель за преградой).
	- Блокировка стрельбы по союзникам и трупам, а также блокировка бесполезных выстрелов в аркадном режиме.
	- Улучшение для перка "Эксперт": теперь данные могут запоминаться, а также автоматически запрашиваться для противника в автоприцеле и т.д. Запросы больше не сбрасываются при снятии фокуса с цели. Они будут кэшироваться и показываться при следующем наведении на цель.
	- Модификация может показывать информацию о текущем состоянии сведения вашего орудия и разбросе.
	- В снайперском режиме доступен маркер упреждения, работает на основе модуля захвата цели модификацией.
	- В модификации имеется альтернативный режим прицеливания для артиллерии, в котором вид не вертикальный, а под углом попадания снаряда в цель. Такой вид позволяет прицельно кидать по неподвижным целям, выбирая наиболее оптимальную точку прицеливания по высоте, а также более объективно оценивать вероятность попадания снаряда за укрытия.

Модификация активно дорабатывается, поэтому не исключена вероятность расширения этого списка.

[Mod description]
Initially mod was developed as automated replace for "Ballistic calculator" mod. However later a large number of new functions was added to modification.
At the moment the latest version of modification (0.0.8 Alpha #2) has the following functionality:
	- Modification can correct the position of an aiming point, thereby liquidating flights over when firing with the big anticipation on quickly moving target (correction of a distance). In more detail in "Algorithm of work: arcade mode".
 	- Modification can lock an aiming point height in the artillery mode, thereby allows to shoot under various shelters when the tank of the opponent isn't visible from above (bridges, etc.).
  	- Modification can raise an aiming point over the ground in the artillery mode for simplification of firing with anticipation on accurate artilleries on the difficult district. In more detail in "Algorithm of work: artillery mode".
  	- Correction of an aiming point in the arcade mode can be carried out in manual and automatic mode, based on the data of targetLock module. In more detail in "Algorithm of work: target locking".
  	- In the artillery mode absolute height is corrected manually, and the relative amendment on height - automatically.
  	- When target is locked and also at correction of an aiming point an additional information can be displayed.
  	- Target capture in autosight can be made with X-ray (through textures at exact hit of a sight in the target behind a barrier).
  	- Blocking of firing on allies and corpses, and also blocking of useless shots in the arcade mode.
  	- Improvement for an "Expert" perk: now data can be remembered, and also automatically be requested for the opponent in an autosight, etc. Replies aren't removed at removal of focus from the target any more. It will be dumped to cache and shown at the following aiming at the target.
  	- Modification can show information on current state of your gun dispersion.
  	- In the sniper mode the anticipation marker is available, works at a basis of the target lock module.
  	- In modification there is an alternative mode of aiming for artillery, in which view is not vertical, but at an angle shell hits the target. Such view allows to throw precisely on standing targets, choosing the most optimum aiming point  and also to estimate probability of shell flies over the target shelter.

Modification is actively developed therefore the probability of extension of this list isn't excluded.

[Важная информация]
Некоторые функции модификации по умолчанию отключены. Для их использования необходимо активировать их в файле конфигурации.
Большинство функционала модификации имеет достаточно большое количество различных настраиваемых параметров.

[Important information]
Some functions of modification are by default disabled. For their use it is necessary to activate them in the configuration file.
The majority of functionality of modification has rather large number of various adjustment parameters.

[Редактирование файла конфигурации]
Файл конфигурации нельзя редактировать стандартным блокнотом, входящим в Windows, а также другими простыми текстовыми редакторами, не поддерживающими кодировку файла конфигурации. Для версий начиная с 0.0.8 это UTF-8 без BOM, и Windows-1251 для более ранних версий модификации.
Все параметры файла конфигурации имеют комментарии на русском и английском языках. Если у параметра нет комментария, значит этот параметр определен выше. Ищите одноименный параметр / секцию.

[Configuration file editing]
The configuration file can't be edited by standard notepad included in Windows, and also other simple text editors which aren't supporting the encoding of the configuration file. For versions since 0.0.8 it is UTF-8 without BOM, and Windows-1251 for earlier versions of modification.
All parameters in the configuration file have comments in the Russian and English languages. If parameter has no comment, this means that it was determined above. Look for the parameter / section of the same name.

[Подробное описание некоторых функций]
Алгоритм работы: захват цели
	Модификация может запоминать танк противника и использовать его параметры для расчета некоторых данных и управления некоторыми модулями. За подобное поведение модификации отвечает модуль targetLock. Данный модуль может работать в ручном (по нажатию клавиши) и автоматическом (при наведении на противника) режиме. Для захвата противника вручную необходимо навести на него прицел и нажать соответствующую клавишу. В автоматическом режиме достаточно просто навести прицел на цель.
	Модуль захвата цели может использовать модуль рентген-захвата (XRayTargetScanner), тем самым, добавляя возможность получать данные о находящихся за преградами целях, видимых союзниками. При захвате цели на экран выводится информация о цели, ее тип и скорость. Данная информация может помочь в выборе
	упреждения, что особенно полезно при игре на артиллерии. Единицы, в которых выводится скорость противника, могут быть изменены и настраиваются в файле конфигурации как множитель.

Алгоритм работы: аркадный режим
	В связи с тем, что все орудия в игре имеют разную скорость полета снаряда при одинаковой гравитации (арта не в счет), настильность стрельбы тоже разная. Система прицеливания в игре наводит орудие в точку пересечения луча, выходящего из камеры игрока через центр прицельной сетки с каким-либо материальным объектом, будь то танк или скала вдали. Также материальной считается физическая граница карты (находится в 50-100 метрах за красной линией по краям карты).
	Наверняка многие из вас пытались стрелять в быстро движущиеся на фоне далеких текстур танки с упреждением и замечали, что вместо попадания в танк снаряд пролетает над противником, вызывая неслабый нагрев вашего стула. Почему же так происходит? И что вы делаете неправильно? Да в том и дело, что все правильно. Просто так устроена система прицеливания в игре. Как я писал выше, как только вы выводите прицел за пределы танка противника, система прицеливания автоматически перенастраивается на стрельбу по текстурам вдалеке за противником. А поскольку расстояние до текстур на заднем плане значительно больше расстояния до противника, то и траектория полета снаряда более навесная. И снаряд, пролетая по более высокой траектории, просто перелетая противника! Таким образом, оригинальная система прицеливания просто не дает вам вести огонь с упреждением! Для стрельбы в таких случаях необходимо наводить орудие не на далекую скалу, а на нематериальную точку впереди прямо по курсу танка. То-есть корректировать дистанцию. Что и делает данный модификация.
	Данная корректировка может осуществляться как в ручном, так и в автоматическом режиме. В обоих режимах на экран выводится информация о захваченной дистанции.
	Ручной режим:
		Наводим орудие на некоторую текстуру, зажимаем клавишу блокировки, дистанция до текстуры запоминается, затем переводим прицел в нужную точку и стреляем, удерживая клавишу блокировки. Например, мы знаем, что противник стоит в кустах рядом с камнем, за который он сразу после выстрела отъезжает. За кустом никаких материальных объектов нет (куст на фоне неба). При попытке выстрелить в куст снаряд просто пролетит над противником и не нанесет ему никакого урона. Наводим прицел на камень, зажимаем клавишу блокировки и, удерживая ее, переводим прицел на куст, туда где стоит противник. Стреляем. Пока клавиша блокировки зажата, модификация удерживает расстояние, при отпускании дистанция сбрасывается и модификация переходит в нормальный режим. Можно сделать даже несколько выстрелов.
	Автоматический режим:
		Для работы данной функции в автоматическом режиме необходимо настроить модуль захвата цели соответствующим образом. При захвате цели модификацией, дистанция будет корректироваться автоматически.

Алгоритм работы: артиллерийский режим
	В вертикальном артиллерийском режиме прицеливания происходит примерно то же самое, что и в аркадном режиме. Только неуправляемым параметром является не расстояние до цели, а высота. Точка прицеливания по прежнему определяется коллижн тестом луча, выходящего из камеры. Поэтому используя стандартную методику прицеливания невозможно навестись под мост, так как прицеливание будет происходить на верхнюю часть моста. Этим часто пользуются сообразительные противники, думая, что там артиллерия их точно не достанет. Тем не менее, никаких препятствий для полета снаряда нет. Как закинуть под мост? Ответ прост - нужно зафиксировать высоту и навестись на противника под мостом. То есть навестись сначала на некоторую точку, абсолютная высота которой эквивалентна высоте поверхности под танком противника. Затем нажать и удерживать кнопку блокировки высоты. Перевести прицел на противника, точнее немного за него (читайте гайды по арте) и произвести выстрел.
	Для стрельбы с упреждением на сложной местности в модификации предусмотрена дополнительная функция - подъем высоты точки прицеливания на некоторую величину, пропорциональную высоте танка противника. Данная функция позволяет стрелять не под гусеницы противнику, а выше - в корпус. Эффективно только на артиллерии с небольшим разбросом и высокой скоростью полета снаряда, особенно при стрельбе бронебойными, подкалиберными (такое встречается) и кумулятивными снарядами, а также фугасами с малым радиусом поражения. Опытные игроки в таких случаях прицеливаются вперед и за танк, с помощью модификации можно просто 	прицеливаться прямо по курсу движения противника.

[Additional description of several functional]
Algorithm of work: target lock
	Modification can remember the tank of the opponent and use its parameters for calculation of some data and control of some modules. The targetLock module is responsible for similar behavior of modification. This module can work in manual (on pressing of a key) and automatic (when aiming at the opponent) the mode. For taking of the opponent manually it is necessary to guide at it a sight and to press the corresponding key. In the automatic mode rather simply guide a sight at the target.
	The module of target lock can use the module of X-ray target lock (XRayTargetScanner), thereby, adding opportunity to obtain data on the target which are behind barriers and seen by allies.
	At target lock an information on target, its type and speed is displayed. This information can help with an anticipation choice that is especially useful at game on artillery. Units in which the opponent's speed is displayed, can be changed and are adjusted in the configuration file as a multiplier.

Algorithm of work: arcade mode
	Because all guns in the game have the different shell speed at identical gravitation (artillery it isn't counted), firing height is different too. The aiming system in game directs the gun in a point of intersection of the beam leaving from the player's camera through the center of an aim grid with any material object, whether tank or rock in the distance. The physical box of the map is considered also material (is in 50-100 meters behind the red line at the edges of the map). For certain many of you tried to shoot at the tanks which are quickly moving in front of far textures with anticipation and noticed, that instead of hit in the tank the shell flies by over the opponent, causing rather strong heating of your chair. Why this occurs? And what are you doing wrong?
	Everything you are doing is correct. The aiming system in game is just like that arranged. As I wrote above, as soon as you move a sight out of contour of the opponent's tank, the aiming system is automatically recustomized on firing onto textures in the distance behind the opponent. And as distance to textures on a background is farther than opponent, and trajectory of flight of a shell more hinged. And a shell, flying by on higher trajectory, simply flying over the opponent! Thus, the original aiming system simply doesn't allow you to fire with anticipation! For firing in such cases it is necessary to direct the gun not at the far rock, but at a non-material point directly in front of enemy tank. Distance correction. As does given modification.
	This adjustment can be carried out both in manual, and in the automatic mode. In both modes information about locked distance appears on the screen.
	Manual mode:
		We direct the gun at some texture, we clamp a blocking key, the distance to texture is remembered, then we move a sight to the necessary point and we shoot, holding a blocking key. For example, we know that the opponent stands in bushes near a stone which it right after a shot drives behind. Behind a bush no material objects are present (a bush in front of the sky). In attempt to shoot at a bush the shell will simply fly by over the opponent and it won't cause it any losses. We direct a sight at a stone, we clamp a key of blocking and, holding it, we move a sight to a bush, there where there is an opponent. We shoot. Until the key of blocking is clamped, modification holds distance, at on releasing the distance is resetted and modification passes into the normal mode. It is possible to make even more than one shot.
	Automatic mode:
		For work of this function in the automatic mode it is necessary to adjust the target lock module. The distance will be automatically corrected, using data of target lock module.

Algorithm of work: artillery mode
	In the vertical artillery aiming mode there is approximately the same, as in the arcade mode. Only uncontrollable parameter is not the distance to the target, but height. The aiming point is still defined by collision test of the beam leaving the camera. Therefore using a standard technique of an aiming it is impossible to aim under the bridge as the aiming will occur on the top part of the bridge. Clever opponents often use it, thinking that there the artillery will definitely not get them. Nevertheless, no obstacles on the shell path are present. How to throw under the bridge? The answer is simple - it is necessary to fix height and be guided at the opponent under the bridge. That is be guided at first at some point which absolute height is equivalent to surface height under the opponent's tank. Then to press and hold the button of locking of a height. Move a sight to the opponent, is more exact a little behind it (read guides on artillery) and to make a shot.
	For firing with anticipation on the difficult district additional function - rise in height of an aiming point at some value proportional to height of the opponent's tank is provided in modifications. This function allows to shoot not under caterpillars to the opponent, but above - at the body. Effectively only on artillery with small dispersion and high speed of shell, especially when firing armor-piercing, subcaliber (such meets) and cumulative shells, and also high explosive shells with a small defeat radius. Skilled players in such cases aim forward and behind the tank, by means of modification it is possible to aim simply directly at the path of moving opponent.

[Автор выражает благодарность за помощь в разработке и тестировании модификации / Thanks for development and testing assistance]
StranikS_Scan, MakcT40, refaol, vasbelous, magiksky709, Scharfhobel, i31, snechinskij, lii
