import py_compile
import traceback
import marshal
import shutil
import struct
import json
import time
import io
import os

def compileSource(source, filename = '<string>', filetime = time.time()):
	with io.BytesIO() as bytesIO:
		bytesIO.write(py_compile.MAGIC)
		bytesIO.write(struct.pack('L', long(filetime)))
		bytesIO.write(marshal.dumps(compile(source, filename, 'exec')))
		result = bytesIO.getvalue()
	return result

def copyLibFiles(library, local, files):
	files = filter(lambda file: os.path.dirname(file) == local, files)
	files = map(os.path.basename, files)
	files = filter(lambda file: os.path.isfile(os.path.join(library, file)), files)
	for file in files:
		shutil.copy2(os.path.join(library, file), local)
	return None

if __name__ == '__main__':
	try:
		with open(__file__.replace('.py', '.cfg'), 'rt') as f:
			config = json.load(f)
		if config["copyLibFiles"]:
			copyLibFiles(config["XModCodeLibraryPath"], config["localXModPath"], config["buildOrder"])
		if config["printBuildOrder"]:
			print '\n'.join(config["buildOrder"]) + '\n'
		sources = []
		for filename in config["buildOrder"]:
			if not os.path.isfile(filename):
				raise IOError('File not found: {}.'.format(filename))
			with open(filename, 'rt') as f:
				sources.append(f.read())
		fullSourceText = '\n'.join(sources)
		if config["createSourceFile"]:
			with open(config["modName"] + '.py', 'wt') as f:
				f.write(fullSourceText)
		with open(config["modName"] + '.pyc', 'wb') as f:
			filetime = os.path.getmtime(config["modName"] + '.py') if os.path.isfile(config["modName"] + '.py') else time.time()
			f.write(compileSource(fullSourceText, config["modName"] + '.py', filetime))
		if config["importCheck"]:
			os.system('python -c "import ' + config["modName"] + '"')
	except:
		traceback.print_exc()
	os.system('pause')
